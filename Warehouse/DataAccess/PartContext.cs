﻿using DataAccess.Entities;
using DataAccess.EntityConfigurations;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading;


namespace DataAccess
{
    public class PartContext : DbContext
    {
        public PartContext():base("Lely")
        {
           // this.Configuration.LazyLoadingEnabled = false;
        }
        public DbSet<PartEntity> Parts { get; set; }
        public DbSet<OrderEntity> Orders { get; set; }
        public DbSet<OrderItemEntity> OrderItems { get; set; }
        public DbSet<BillEntity> Bills { get; set; }
        public DbSet<BillItemEntity> BillItems { get; set; }
        public DbSet<CustomerEntity> Customers { get; set; }
        public DbSet<UnitCountEntity> UnitCounts { get; set; }
        public DbSet<OperationEntity> Operations { get; set; }
        public DbSet<BatchOfMaterialEntity>  BatchOfMaterials { get; set; }
        public DbSet<CategoryEntity> PartCategories { get; set; }
        public  DbSet<LelyPartEntity> LelyParts { get; set; }
        public DbSet<DocumentEntity> Documents { get; set; }
        public DbSet<DocumentTypeEntity> DocumentTypes { get; set; }
        public DbSet<WarehouseEntity> Warehouses { get; set; }
        public DbSet<ContoEntity> Contos { get; set; }
        public DbSet<MOLEntity> MOLs { get; set; }
        public DbSet<DocumentItemEntity> DocumentItems { get; set; }
        public DbSet<BalanceEntity> Balances { get; set; }
        //    public DbSet<CategoryEntity> PartCategories { get; set; }
        

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new OperationEntityConfiguration());
            modelBuilder.Configurations.Add(new PartEntityConfiguration());
            modelBuilder.Configurations.Add(new OrderEntityConfiguration());
            modelBuilder.Configurations.Add(new OrderItemEntityConfiguration());
            modelBuilder.Configurations.Add(new BillItemEntityConfiguration());
            modelBuilder.Configurations.Add(new CustomerEntityConfiguration());
            modelBuilder.Configurations.Add(new BillEntityConfiguration());
            modelBuilder.Configurations.Add(new UnitCountEnityConfiguration());
            modelBuilder.Configurations.Add(new BatchOfMaterialEntityConfiguration());
            modelBuilder.Configurations.Add(new CategoryEntityConfiguration());
            modelBuilder.Configurations.Add(new LelyPartEntityConfiguration());
            modelBuilder.Configurations.Add(new DocumentEntityConfiguration());
            modelBuilder.Configurations.Add(new DocumentTypeEntityConfiguration());
            modelBuilder.Configurations.Add(new WarehouseEntityConfiguration());
            modelBuilder.Configurations.Add(new ContoEntityConfiguration());
            modelBuilder.Configurations.Add(new MOLEntityConfiguration());
            modelBuilder.Configurations.Add(new DocumentItemEntityConfiguration());
            modelBuilder.Configurations.Add(new BalanceEntityConfiguration());
        }
        //public override int SaveChanges()
        //{
        //    var modifiedEntries = ChangeTracker.Entries()
        //        .Where(x => x.Entity is IAuditableEntity
        //            && (x.State == System.Data.Entity.EntityState.Added || x.State == System.Data.Entity.EntityState.Modified));

        //    foreach (var entry in modifiedEntries)
        //    {
        //        IAuditableEntity entity = entry.Entity as IAuditableEntity;
        //        if (entity != null)
        //        {
        //            string identityName = Thread.CurrentPrincipal.Identity.Name;
        //            DateTime now = DateTime.UtcNow;

        //            if (entry.State == System.Data.Entity.EntityState.Added)
        //            {
        //                entity.CreatedBy = identityName;
        //                entity.CreatedDate = now;
        //            }
        //            else
        //            {
        //                base.Entry(entity).Property(x => x.CreatedBy).IsModified = false;
        //                base.Entry(entity).Property(x => x.CreatedDate).IsModified = false;
        //            }

        //            entity.UpdatedBy = identityName;
        //            entity.UpdatedDate = now;
        //        }
        //    }

        //    return base.SaveChanges();
        //}
    }
}
