namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BatchOfMaterialEntityAddedWithDecimal : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.BatchOfMaterials", "Quantity", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.BatchOfMaterials", "Quantity", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
    }
}
