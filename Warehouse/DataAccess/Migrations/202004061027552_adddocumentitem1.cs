namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class adddocumentitem1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Documents", "ContoEntityId", "dbo.Contos");
            DropForeignKey("dbo.Documents", "MOLEntityId", "dbo.MOLs");
            DropForeignKey("dbo.Documents", "WarehouseEntityId", "dbo.Warehouses");
            DropIndex("dbo.Documents", new[] { "WarehouseEntityId" });
            DropIndex("dbo.Documents", new[] { "ContoEntityId" });
            DropIndex("dbo.Documents", new[] { "MOLEntityId" });
            AddColumn("dbo.DocumentItems", "WarehouseRecieverId", c => c.Guid());
            AddColumn("dbo.DocumentItems", "WarehouseTransferId", c => c.Guid());
            AddColumn("dbo.Warehouses", "BalanceEntityId", c => c.Guid());
            AddColumn("dbo.Warehouses", "MolEntityId", c => c.Guid());
            AddColumn("dbo.Warehouses", "ContoEntityId", c => c.Guid());
            CreateIndex("dbo.DocumentItems", "WarehouseRecieverId");
            CreateIndex("dbo.DocumentItems", "WarehouseTransferId");
            CreateIndex("dbo.Warehouses", "MolEntityId");
            CreateIndex("dbo.Warehouses", "ContoEntityId");
            AddForeignKey("dbo.Warehouses", "ContoEntityId", "dbo.Contos", "Id");
            AddForeignKey("dbo.Warehouses", "MolEntityId", "dbo.MOLs", "Id");
            AddForeignKey("dbo.DocumentItems", "WarehouseRecieverId", "dbo.Warehouses", "Id");
            AddForeignKey("dbo.DocumentItems", "WarehouseTransferId", "dbo.Warehouses", "Id");
            DropColumn("dbo.Documents", "WarehouseEntityId");
            DropColumn("dbo.Documents", "ContoEntityId");
            DropColumn("dbo.Documents", "MOLEntityId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Documents", "MOLEntityId", c => c.Guid(nullable: false));
            AddColumn("dbo.Documents", "ContoEntityId", c => c.Guid(nullable: false));
            AddColumn("dbo.Documents", "WarehouseEntityId", c => c.Guid(nullable: false));
            DropForeignKey("dbo.DocumentItems", "WarehouseTransferId", "dbo.Warehouses");
            DropForeignKey("dbo.DocumentItems", "WarehouseRecieverId", "dbo.Warehouses");
            DropForeignKey("dbo.Warehouses", "MolEntityId", "dbo.MOLs");
            DropForeignKey("dbo.Warehouses", "ContoEntityId", "dbo.Contos");
            DropIndex("dbo.Warehouses", new[] { "ContoEntityId" });
            DropIndex("dbo.Warehouses", new[] { "MolEntityId" });
            DropIndex("dbo.DocumentItems", new[] { "WarehouseTransferId" });
            DropIndex("dbo.DocumentItems", new[] { "WarehouseRecieverId" });
            DropColumn("dbo.Warehouses", "ContoEntityId");
            DropColumn("dbo.Warehouses", "MolEntityId");
            DropColumn("dbo.Warehouses", "BalanceEntityId");
            DropColumn("dbo.DocumentItems", "WarehouseTransferId");
            DropColumn("dbo.DocumentItems", "WarehouseRecieverId");
            CreateIndex("dbo.Documents", "MOLEntityId");
            CreateIndex("dbo.Documents", "ContoEntityId");
            CreateIndex("dbo.Documents", "WarehouseEntityId");
            AddForeignKey("dbo.Documents", "WarehouseEntityId", "dbo.Warehouses", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Documents", "MOLEntityId", "dbo.MOLs", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Documents", "ContoEntityId", "dbo.Contos", "Id", cascadeDelete: true);
        }
    }
}
