// <auto-generated />
namespace DataAccess.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class NotNullableKeyBillItems : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(NotNullableKeyBillItems));
        
        string IMigrationMetadata.Id
        {
            get { return "201910180737334_NotNullableKeyBillItems"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
