namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DateorderDelete : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Orders", "CreateDate");
            DropColumn("dbo.Orders", "ModifiedDate");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Orders", "ModifiedDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Orders", "CreateDate", c => c.DateTime(nullable: false));
        }
    }
}
