namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddLimitOnWH : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Parts", "LimitQuantity", c => c.Double());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Parts", "LimitQuantity");
        }
    }
}
