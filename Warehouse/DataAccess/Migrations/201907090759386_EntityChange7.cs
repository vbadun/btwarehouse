namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EntityChange7 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Orders", "OrderItemsEntityId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Orders", "OrderItemsEntityId", c => c.Guid(nullable: false));
        }
    }
}
