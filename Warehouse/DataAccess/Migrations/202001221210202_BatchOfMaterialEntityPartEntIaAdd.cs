namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BatchOfMaterialEntityPartEntIaAdd : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.BatchOfMaterials", "PartEntity_Id", "dbo.Parts");
            DropIndex("dbo.BatchOfMaterials", new[] { "PartEntity_Id" });
            RenameColumn(table: "dbo.BatchOfMaterials", name: "PartEntity_Id", newName: "PartEntityId");
            AlterColumn("dbo.BatchOfMaterials", "PartEntityId", c => c.Guid(nullable: false));
            CreateIndex("dbo.BatchOfMaterials", "PartEntityId");
            AddForeignKey("dbo.BatchOfMaterials", "PartEntityId", "dbo.Parts", "Id", cascadeDelete: true);
            DropColumn("dbo.BatchOfMaterials", "PartId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.BatchOfMaterials", "PartId", c => c.Guid(nullable: false));
            DropForeignKey("dbo.BatchOfMaterials", "PartEntityId", "dbo.Parts");
            DropIndex("dbo.BatchOfMaterials", new[] { "PartEntityId" });
            AlterColumn("dbo.BatchOfMaterials", "PartEntityId", c => c.Guid());
            RenameColumn(table: "dbo.BatchOfMaterials", name: "PartEntityId", newName: "PartEntity_Id");
            CreateIndex("dbo.BatchOfMaterials", "PartEntity_Id");
            AddForeignKey("dbo.BatchOfMaterials", "PartEntity_Id", "dbo.Parts", "Id");
        }
    }
}
