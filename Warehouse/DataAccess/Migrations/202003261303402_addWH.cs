namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addWH : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Contos",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.MOLs",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Warehouses",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Documents", "WarehouseEntityId", c => c.Guid(nullable: false));
            AddColumn("dbo.Documents", "ContoEntityId", c => c.Guid(nullable: false));
            AddColumn("dbo.Documents", "MOLEntityId", c => c.Guid(nullable: false));
            CreateIndex("dbo.Documents", "WarehouseEntityId");
            CreateIndex("dbo.Documents", "ContoEntityId");
            CreateIndex("dbo.Documents", "MOLEntityId");
            AddForeignKey("dbo.Documents", "ContoEntityId", "dbo.Contos", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Documents", "MOLEntityId", "dbo.MOLs", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Documents", "WarehouseEntityId", "dbo.Warehouses", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Documents", "WarehouseEntityId", "dbo.Warehouses");
            DropForeignKey("dbo.Documents", "MOLEntityId", "dbo.MOLs");
            DropForeignKey("dbo.Documents", "ContoEntityId", "dbo.Contos");
            DropIndex("dbo.Documents", new[] { "MOLEntityId" });
            DropIndex("dbo.Documents", new[] { "ContoEntityId" });
            DropIndex("dbo.Documents", new[] { "WarehouseEntityId" });
            DropColumn("dbo.Documents", "MOLEntityId");
            DropColumn("dbo.Documents", "ContoEntityId");
            DropColumn("dbo.Documents", "WarehouseEntityId");
            DropTable("dbo.Warehouses");
            DropTable("dbo.MOLs");
            DropTable("dbo.Contos");
        }
    }
}
