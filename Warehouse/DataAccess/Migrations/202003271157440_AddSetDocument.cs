namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSetDocument : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Documents", "TakeStock", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Documents", "TakeStock");
        }
    }
}
