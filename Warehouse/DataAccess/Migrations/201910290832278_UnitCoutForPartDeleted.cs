namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UnitCoutForPartDeleted : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Parts", "UnitCount");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Parts", "UnitCount", c => c.String());
        }
    }
}
