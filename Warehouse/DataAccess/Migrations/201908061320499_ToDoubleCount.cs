namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ToDoubleCount : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.OrderItems", "Count", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.OrderItems", "Count", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
    }
}
