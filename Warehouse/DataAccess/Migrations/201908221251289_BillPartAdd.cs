namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BillPartAdd : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BillItems", "PartEntityId", c => c.Guid());
            CreateIndex("dbo.BillItems", "PartEntityId");
            AddForeignKey("dbo.BillItems", "PartEntityId", "dbo.Parts", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BillItems", "PartEntityId", "dbo.Parts");
            DropIndex("dbo.BillItems", new[] { "PartEntityId" });
            DropColumn("dbo.BillItems", "PartEntityId");
        }
    }
}
