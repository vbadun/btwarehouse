namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UnitCoutRelationShipsForPartAddedNullable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Parts", "UnitCountId", "dbo.UnitCounts");
            DropIndex("dbo.Parts", new[] { "UnitCountId" });
            AlterColumn("dbo.Parts", "UnitCountId", c => c.Guid());
            CreateIndex("dbo.Parts", "UnitCountId");
            AddForeignKey("dbo.Parts", "UnitCountId", "dbo.UnitCounts", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Parts", "UnitCountId", "dbo.UnitCounts");
            DropIndex("dbo.Parts", new[] { "UnitCountId" });
            AlterColumn("dbo.Parts", "UnitCountId", c => c.Guid(nullable: false));
            CreateIndex("dbo.Parts", "UnitCountId");
            AddForeignKey("dbo.Parts", "UnitCountId", "dbo.UnitCounts", "Id", cascadeDelete: true);
        }
    }
}
