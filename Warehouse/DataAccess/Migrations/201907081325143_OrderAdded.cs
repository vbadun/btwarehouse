namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrderAdded : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.OrderItems",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        PartId = c.Guid(nullable: false),
                        Count = c.Int(nullable: false),
                        OrderEntity_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Orders", t => t.OrderEntity_Id)
                .Index(t => t.OrderEntity_Id);
            
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OrderItems", "OrderEntity_Id", "dbo.Orders");
            DropIndex("dbo.OrderItems", new[] { "OrderEntity_Id" });
           
            DropTable("dbo.Orders");
            DropTable("dbo.OrderItems");
        }
    }
}
