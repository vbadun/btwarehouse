namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EntityChange2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.OrderItems", "OrderEntiytyId", c => c.Guid(nullable: false));
            DropColumn("dbo.OrderItems", "OrderEntiytId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.OrderItems", "OrderEntiytId", c => c.Guid(nullable: false));
            DropColumn("dbo.OrderItems", "OrderEntiytyId");
        }
    }
}
