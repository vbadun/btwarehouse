namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EntityChange : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.OrderItems", "OrderEntiytId", c => c.Guid(nullable: false));
            AddColumn("dbo.OrderItems", "ParEntiytId", c => c.Guid(nullable: false));
            DropColumn("dbo.OrderItems", "PartId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.OrderItems", "PartId", c => c.Guid(nullable: false));
            DropColumn("dbo.OrderItems", "ParEntiytId");
            DropColumn("dbo.OrderItems", "OrderEntiytId");
        }
    }
}
