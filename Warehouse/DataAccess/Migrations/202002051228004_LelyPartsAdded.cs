namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LelyPartsAdded : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LelyParts",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        EngName = c.String(),
                        PartNumber = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.LelyParts");
        }
    }
}
