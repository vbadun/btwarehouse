namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EntityChange5 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.OrderItems", "PartEntitytId", c => c.Guid(nullable: false));
            DropColumn("dbo.OrderItems", "PartEntiytId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.OrderItems", "PartEntiytId", c => c.Guid(nullable: false));
            DropColumn("dbo.OrderItems", "PartEntitytId");
        }
    }
}
