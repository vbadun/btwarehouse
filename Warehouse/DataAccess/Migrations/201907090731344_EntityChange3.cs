namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EntityChange3 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.OrderItems", "OrderEntity_Id", "dbo.Orders");
            DropIndex("dbo.OrderItems", new[] { "OrderEntity_Id" });
            RenameColumn(table: "dbo.OrderItems", name: "OrderEntity_Id", newName: "OrderEntityId");
            AddColumn("dbo.OrderItems", "PartEntiytId", c => c.Guid(nullable: false));
            AddColumn("dbo.OrderItems", "PartEntity_Id", c => c.Guid());
            AlterColumn("dbo.OrderItems", "OrderEntityId", c => c.Guid(nullable: false));
            CreateIndex("dbo.OrderItems", "OrderEntityId");
            CreateIndex("dbo.OrderItems", "PartEntity_Id");
            AddForeignKey("dbo.OrderItems", "PartEntity_Id", "dbo.Parts", "Id");
            AddForeignKey("dbo.OrderItems", "OrderEntityId", "dbo.Orders", "Id", cascadeDelete: true);
            DropColumn("dbo.OrderItems", "OrderEntiytyId");
            DropColumn("dbo.OrderItems", "ParEntiytId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.OrderItems", "ParEntiytId", c => c.Guid(nullable: false));
            AddColumn("dbo.OrderItems", "OrderEntiytyId", c => c.Guid(nullable: false));
            DropForeignKey("dbo.OrderItems", "OrderEntityId", "dbo.Orders");
            DropForeignKey("dbo.OrderItems", "PartEntity_Id", "dbo.Parts");
            DropIndex("dbo.OrderItems", new[] { "PartEntity_Id" });
            DropIndex("dbo.OrderItems", new[] { "OrderEntityId" });
            AlterColumn("dbo.OrderItems", "OrderEntityId", c => c.Guid());
            DropColumn("dbo.OrderItems", "PartEntity_Id");
            DropColumn("dbo.OrderItems", "PartEntiytId");
            RenameColumn(table: "dbo.OrderItems", name: "OrderEntityId", newName: "OrderEntity_Id");
            CreateIndex("dbo.OrderItems", "OrderEntity_Id");
            AddForeignKey("dbo.OrderItems", "OrderEntity_Id", "dbo.Orders", "Id");
        }
    }
}
