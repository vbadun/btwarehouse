namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DateAdd : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Bills", "DateOfCreate", c => c.DateTime(nullable: true, storeType: "date"));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Bills", "DateOfCreate");
        }
    }
}
