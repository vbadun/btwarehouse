// <auto-generated />
namespace DataAccess.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class adddocuments : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(adddocuments));
        
        string IMigrationMetadata.Id
        {
            get { return "202003261232307_adddocuments"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
