namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UnitCoutRelationShipsForPartAdded : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UnitCountEntities",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        UnitCountName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Parts", "UnitCountId", c => c.Guid(nullable: true));
            CreateIndex("dbo.Parts", "UnitCountId");
            AddForeignKey("dbo.Parts", "UnitCountId", "dbo.UnitCountEntities", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Parts", "UnitCountId", "dbo.UnitCountEntities");
            DropIndex("dbo.Parts", new[] { "UnitCountId" });
            DropColumn("dbo.Parts", "UnitCountId");
            DropTable("dbo.UnitCountEntities");
        }
    }
}
