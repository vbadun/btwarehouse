namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UnitCoutForPartAdded : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Parts", "UnitCount", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Parts", "UnitCount");
        }
    }
}
