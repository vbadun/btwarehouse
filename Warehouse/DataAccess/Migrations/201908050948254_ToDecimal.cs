namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ToDecimal : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.OrderItems", "Count", c => c.Decimal(nullable: false, precision: 18, scale: 5));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.OrderItems", "Count", c => c.Int(nullable: false));
        }
    }
}
