namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddOperationTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Operations",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        PartEntityId = c.Guid(nullable: false),
                        Quantity = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Parts", t => t.PartEntityId, cascadeDelete: true)
                .Index(t => t.PartEntityId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Operations", "PartEntityId", "dbo.Parts");
            DropIndex("dbo.Operations", new[] { "PartEntityId" });
            DropTable("dbo.Operations");
        }
    }
}
