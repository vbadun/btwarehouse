namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addDocumentItems : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DocumentItems",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        DocumentEntityId = c.Guid(nullable: false),
                        PartEntityId = c.Guid(nullable: false),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Documents", t => t.DocumentEntityId, cascadeDelete: true)
                .ForeignKey("dbo.Parts", t => t.PartEntityId, cascadeDelete: true)
                .Index(t => t.DocumentEntityId)
                .Index(t => t.PartEntityId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DocumentItems", "PartEntityId", "dbo.Parts");
            DropForeignKey("dbo.DocumentItems", "DocumentEntityId", "dbo.Documents");
            DropIndex("dbo.DocumentItems", new[] { "PartEntityId" });
            DropIndex("dbo.DocumentItems", new[] { "DocumentEntityId" });
            DropTable("dbo.DocumentItems");
        }
    }
}
