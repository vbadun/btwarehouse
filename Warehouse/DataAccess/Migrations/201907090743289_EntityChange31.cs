namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EntityChange31 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "OrderItemsEntityId", c => c.Guid(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "OrderItemsEntityId");
        }
    }
}
