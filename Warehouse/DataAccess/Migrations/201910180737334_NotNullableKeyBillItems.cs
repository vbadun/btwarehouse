namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NotNullableKeyBillItems : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.BillItems", "BillEntityId", "dbo.Bills");
            DropForeignKey("dbo.BillItems", "PartEntityId", "dbo.Parts");
            DropForeignKey("dbo.Bills", "CustomerId", "dbo.Customers");
            DropIndex("dbo.BillItems", new[] { "BillEntityId" });
            DropIndex("dbo.BillItems", new[] { "PartEntityId" });
            DropIndex("dbo.Bills", new[] { "CustomerId" });
            AlterColumn("dbo.BillItems", "BillEntityId", c => c.Guid(nullable: false));
            AlterColumn("dbo.BillItems", "PartEntityId", c => c.Guid(nullable: false));
            AlterColumn("dbo.Bills", "CustomerId", c => c.Guid(nullable: false));
            CreateIndex("dbo.BillItems", "BillEntityId");
            CreateIndex("dbo.BillItems", "PartEntityId");
            CreateIndex("dbo.Bills", "CustomerId");
            AddForeignKey("dbo.BillItems", "BillEntityId", "dbo.Bills", "Id", cascadeDelete: true);
            AddForeignKey("dbo.BillItems", "PartEntityId", "dbo.Parts", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Bills", "CustomerId", "dbo.Customers", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Bills", "CustomerId", "dbo.Customers");
            DropForeignKey("dbo.BillItems", "PartEntityId", "dbo.Parts");
            DropForeignKey("dbo.BillItems", "BillEntityId", "dbo.Bills");
            DropIndex("dbo.Bills", new[] { "CustomerId" });
            DropIndex("dbo.BillItems", new[] { "PartEntityId" });
            DropIndex("dbo.BillItems", new[] { "BillEntityId" });
            AlterColumn("dbo.Bills", "CustomerId", c => c.Guid());
            AlterColumn("dbo.BillItems", "PartEntityId", c => c.Guid());
            AlterColumn("dbo.BillItems", "BillEntityId", c => c.Guid());
            CreateIndex("dbo.Bills", "CustomerId");
            CreateIndex("dbo.BillItems", "PartEntityId");
            CreateIndex("dbo.BillItems", "BillEntityId");
            AddForeignKey("dbo.Bills", "CustomerId", "dbo.Customers", "Id");
            AddForeignKey("dbo.BillItems", "PartEntityId", "dbo.Parts", "Id");
            AddForeignKey("dbo.BillItems", "BillEntityId", "dbo.Bills", "Id");
        }
    }
}
