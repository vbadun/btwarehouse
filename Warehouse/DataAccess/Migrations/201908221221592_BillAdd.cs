namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BillAdd : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BillItems",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        BillEntityId = c.Guid(),
                        Count = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Bills", t => t.BillEntityId)
                .Index(t => t.BillEntityId);
            
            CreateTable(
                "dbo.Bills",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Number = c.Int(nullable: false),
                        CustomerId = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Customers", t => t.CustomerId)
                .Index(t => t.CustomerId);
            
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        CustomerName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Bills", "CustomerId", "dbo.Customers");
            DropForeignKey("dbo.BillItems", "BillEntityId", "dbo.Bills");
            DropIndex("dbo.Bills", new[] { "CustomerId" });
            DropIndex("dbo.BillItems", new[] { "BillEntityId" });
            DropTable("dbo.Customers");
            DropTable("dbo.Bills");
            DropTable("dbo.BillItems");
        }
    }
}
