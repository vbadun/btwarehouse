namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SerialIndexADD2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.OrderItems", "SerialIndex", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.OrderItems", "SerialIndex");
        }
    }
}
