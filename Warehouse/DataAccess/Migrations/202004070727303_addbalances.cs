namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addbalances : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Balances",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.Warehouses", "BalanceEntityId");
            AddForeignKey("dbo.Warehouses", "BalanceEntityId", "dbo.Balances", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Warehouses", "BalanceEntityId", "dbo.Balances");
            DropIndex("dbo.Warehouses", new[] { "BalanceEntityId" });
            DropTable("dbo.Balances");
        }
    }
}
