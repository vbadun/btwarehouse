namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Parts", "PictureName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Parts", "PictureName");
        }
    }
}
