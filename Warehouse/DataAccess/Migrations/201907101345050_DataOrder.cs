namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DataOrder : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "ModifiedDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "ModifiedDate");
        }
    }
}
