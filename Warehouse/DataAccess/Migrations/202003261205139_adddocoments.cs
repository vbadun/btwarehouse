namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class adddocoments : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Documents",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        DocumentTypeEntityId = c.Guid(nullable: false),
                        Name = c.String(),
                        DocumentDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DocumentTypes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DocumentTypeEntityDocumentEntities",
                c => new
                    {
                        DocumentTypeEntity_Id = c.Guid(nullable: false),
                        DocumentEntity_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.DocumentTypeEntity_Id, t.DocumentEntity_Id })
                .ForeignKey("dbo.DocumentTypes", t => t.DocumentTypeEntity_Id, cascadeDelete: true)
                .ForeignKey("dbo.Documents", t => t.DocumentEntity_Id, cascadeDelete: true)
                .Index(t => t.DocumentTypeEntity_Id)
                .Index(t => t.DocumentEntity_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DocumentTypeEntityDocumentEntities", "DocumentEntity_Id", "dbo.Documents");
            DropForeignKey("dbo.DocumentTypeEntityDocumentEntities", "DocumentTypeEntity_Id", "dbo.DocumentTypes");
            DropIndex("dbo.DocumentTypeEntityDocumentEntities", new[] { "DocumentEntity_Id" });
            DropIndex("dbo.DocumentTypeEntityDocumentEntities", new[] { "DocumentTypeEntity_Id" });
            DropTable("dbo.DocumentTypeEntityDocumentEntities");
            DropTable("dbo.DocumentTypes");
            DropTable("dbo.Documents");
        }
    }
}
