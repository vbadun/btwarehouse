namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EntityChange6 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.OrderItems", "PartEntity_Id", "dbo.Parts");
            DropIndex("dbo.OrderItems", new[] { "PartEntity_Id" });
            RenameColumn(table: "dbo.OrderItems", name: "PartEntity_Id", newName: "PartEntityId");
            AlterColumn("dbo.OrderItems", "PartEntityId", c => c.Guid(nullable: false));
            CreateIndex("dbo.OrderItems", "PartEntityId");
            AddForeignKey("dbo.OrderItems", "PartEntityId", "dbo.Parts", "Id", cascadeDelete: true);
            DropColumn("dbo.OrderItems", "PartEntitytId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.OrderItems", "PartEntitytId", c => c.Guid(nullable: false));
            DropForeignKey("dbo.OrderItems", "PartEntityId", "dbo.Parts");
            DropIndex("dbo.OrderItems", new[] { "PartEntityId" });
            AlterColumn("dbo.OrderItems", "PartEntityId", c => c.Guid());
            RenameColumn(table: "dbo.OrderItems", name: "PartEntityId", newName: "PartEntity_Id");
            CreateIndex("dbo.OrderItems", "PartEntity_Id");
            AddForeignKey("dbo.OrderItems", "PartEntity_Id", "dbo.Parts", "Id");
        }
    }
}
