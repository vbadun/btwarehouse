namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class adddocuments : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.DocumentTypeEntityDocumentEntities", "DocumentTypeEntity_Id", "dbo.DocumentTypes");
            DropForeignKey("dbo.DocumentTypeEntityDocumentEntities", "DocumentEntity_Id", "dbo.Documents");
            DropIndex("dbo.DocumentTypeEntityDocumentEntities", new[] { "DocumentTypeEntity_Id" });
            DropIndex("dbo.DocumentTypeEntityDocumentEntities", new[] { "DocumentEntity_Id" });
            CreateIndex("dbo.Documents", "DocumentTypeEntityId");
            AddForeignKey("dbo.Documents", "DocumentTypeEntityId", "dbo.DocumentTypes", "Id", cascadeDelete: true);
            DropTable("dbo.DocumentTypeEntityDocumentEntities");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.DocumentTypeEntityDocumentEntities",
                c => new
                    {
                        DocumentTypeEntity_Id = c.Guid(nullable: false),
                        DocumentEntity_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.DocumentTypeEntity_Id, t.DocumentEntity_Id });
            
            DropForeignKey("dbo.Documents", "DocumentTypeEntityId", "dbo.DocumentTypes");
            DropIndex("dbo.Documents", new[] { "DocumentTypeEntityId" });
            CreateIndex("dbo.DocumentTypeEntityDocumentEntities", "DocumentEntity_Id");
            CreateIndex("dbo.DocumentTypeEntityDocumentEntities", "DocumentTypeEntity_Id");
            AddForeignKey("dbo.DocumentTypeEntityDocumentEntities", "DocumentEntity_Id", "dbo.Documents", "Id", cascadeDelete: true);
            AddForeignKey("dbo.DocumentTypeEntityDocumentEntities", "DocumentTypeEntity_Id", "dbo.DocumentTypes", "Id", cascadeDelete: true);
        }
    }
}
