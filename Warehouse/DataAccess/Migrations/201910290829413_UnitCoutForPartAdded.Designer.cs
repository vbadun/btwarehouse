// <auto-generated />
namespace DataAccess.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class UnitCoutForPartAdded : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(UnitCoutForPartAdded));
        
        string IMigrationMetadata.Id
        {
            get { return "201910290829413_UnitCoutForPartAdded"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
