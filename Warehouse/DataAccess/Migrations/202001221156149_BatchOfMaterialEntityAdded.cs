namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BatchOfMaterialEntityAdded : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BatchOfMaterials",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        PartId = c.Guid(nullable: false),
                        DateOfBatch = c.DateTime(nullable: false),
                        Quantity = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PartEntity_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Parts", t => t.PartEntity_Id)
                .Index(t => t.PartEntity_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BatchOfMaterials", "PartEntity_Id", "dbo.Parts");
            DropIndex("dbo.BatchOfMaterials", new[] { "PartEntity_Id" });
            DropTable("dbo.BatchOfMaterials");
        }
    }
}
