namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addcategories : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PartCategories",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Parts", "CategoryEntityId", c => c.Guid());
            CreateIndex("dbo.Parts", "CategoryEntityId");
            AddForeignKey("dbo.Parts", "CategoryEntityId", "dbo.PartCategories", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Parts", "CategoryEntityId", "dbo.PartCategories");
            DropIndex("dbo.Parts", new[] { "CategoryEntityId" });
            DropColumn("dbo.Parts", "CategoryEntityId");
            DropTable("dbo.PartCategories");
        }
    }
}
