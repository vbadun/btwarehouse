﻿using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class OrderManager
    {
        private PartContext db = new PartContext();

        public void CreateOrder()
        {
            var part1 = db.Parts.Where(x => x.PartNumber == "5.1004.0420.0").SingleOrDefault();
            var part2 = db.Parts.Where(x => x.PartNumber == "9.1138.0810.0").SingleOrDefault();
            var order = new OrderEntity()
            {
                Id = Guid.NewGuid(),
                Name = "First Order"

            };
            var oi1 = new OrderItemEntity()
            {
                Id = Guid.NewGuid(),
                PartEntityId = part1.Id,
                Count = 3,
                OrderEntityId = order.Id
            };
            var oi2 = new OrderItemEntity()
            {
                Id = Guid.NewGuid(),
                PartEntityId = part2.Id,
                Count = 8,
                OrderEntityId = order.Id
            };
            db.Orders.Add(order);
            db.OrderItems.Add(oi1);
            db.OrderItems.Add(oi2);
            db.SaveChanges();
        }

    }
}
