﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Entities
{
   public class DocumentItemEntity
    {
        public Guid Id { get; set; }
        public Guid DocumentEntityId { get; set; }
        public DocumentEntity Document { get; set; }
        public Guid PartEntityId { get; set; }
        public virtual PartEntity Part { get; set; }
        public decimal Amount { get; set; }
        public Guid? WarehouseRecieverId { get; set; }
        public Guid? WarehouseTransferId { get; set; }
        public virtual WarehouseEntity WarehouseReciever { get; set; }
        public virtual WarehouseEntity WarehouseTransfer { get; set; }
    }
}
