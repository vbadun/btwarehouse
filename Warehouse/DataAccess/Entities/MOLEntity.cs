﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Entities
{
   public class MOLEntity
    {
        public Guid Id { get; set; }
        [Display(Name = "МОЛ")]
        public string Name { get; set; }
        public virtual ICollection<WarehouseEntity> Warehouses { get; set; }
    }
}
