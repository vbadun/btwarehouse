﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Entities
{
    public class PartEntity 
    {
        public Guid Id { get; set; }
        public Guid? UnitCountId { get; set; }
        public Guid? CategoryEntityId { get; set; }
        [Display(Name = "Единицы измерения")]
        public virtual UnitCountEntity UnitCount{ get; set; } //Единицы измеренения
        [Display(Name = "Категория")]
        public virtual CategoryEntity Category { get; set; }
        [Display(Name = "Наименование")]
        public string RusName { get; set; }
        public string EngName { get; set; }
        [Display(Name = "Мин лимит на складе")]
        public double? LimitQuantity { get; set; }
        [Display(Name = "Артикул")]
        public string PartNumber { get; set; }
        public string PictureName { get; set; }
        public virtual ICollection<OrderItemEntity> OrderItems { get; set; }
        public virtual ICollection<BillItemEntity> BillItems { get; set; }
        public virtual ICollection<OperationEntity> Operations { get; set; }
        public virtual ICollection<BatchOfMaterialEntity> BatchOfMaterials { get; set; }
        public virtual ICollection<DocumentItemEntity> DocumentItems { get; set; }
    }
}
