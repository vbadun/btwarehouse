﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Entities
{
    public class DocumentEntity
    {
        public Guid Id { get; set; }
        public Guid DocumentTypeEntityId { get; set; }
        public string Name { get; set; }
        public bool? TakeStock { get; set; }
        public DateTime? DocumentDate { get; set; }
        public DocumentTypeEntity DocumentType { get; set; }
        public virtual ICollection<DocumentItemEntity> DocumentItems { get; set; }
    }
}
