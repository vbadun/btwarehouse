﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Entities
{
    public class CustomerEntity
    {
        public Guid Id { get; set; }
        public string CustomerName { get; set; }

        public virtual ICollection<BillEntity> Bills { get; set; }
    }
}
