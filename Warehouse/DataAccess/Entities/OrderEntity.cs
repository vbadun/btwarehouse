﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Entities
{
    public class OrderEntity
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime? OrderDate { get; set; }
        public virtual ICollection<OrderItemEntity> OrderItems { get; set; }
    } 
}
