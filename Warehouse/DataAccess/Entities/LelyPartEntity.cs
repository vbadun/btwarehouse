﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Entities
{
    public class LelyPartEntity
    {
        public Guid Id { get; set; }
        public string EngName { get; set; }
        public string PartNumber { get; set; }
    }
}
