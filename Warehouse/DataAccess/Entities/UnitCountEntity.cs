﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Entities
{
    public class UnitCountEntity
    {
            public Guid Id { get; set; }
            [Display(Name = "Ед.изм.")]
            public string UnitCountName { get; set; }
            public virtual ICollection<PartEntity> Parts { get; set; }
    }
}
