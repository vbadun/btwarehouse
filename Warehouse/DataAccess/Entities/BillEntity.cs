﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Entities
{
   public class BillEntity
    {
        public Guid Id { get; set; }
        public int Number { get; set; }
        [Column(TypeName = "Date")]
        public DateTime DateOfCreate { get; set; }

        //Relationships
        public Guid CustomerId { get; set; }
        public virtual CustomerEntity Customer { get; set; }
        public virtual ICollection<BillItemEntity> BillItems { get; set; }
    }
}
