﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Entities
{
    public class BatchOfMaterialEntity
    {
        public Guid Id { get; set; }
        public Guid PartEntityId { get; set; }
        public DateTime DateOfBatch { get; set; }
        public double Quantity { get; set; }
        public virtual PartEntity PartEntity { get; set; }
    }
}
