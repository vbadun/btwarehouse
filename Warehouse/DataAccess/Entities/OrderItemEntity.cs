﻿using System;

namespace DataAccess.Entities
{
    public class OrderItemEntity
    {
        public Guid Id { get; set; }
        public Guid OrderEntityId { get; set; }
        public Guid PartEntityId { get; set; }
        public int? SerialIndex { get; set; }
        public double Count { get; set; }
        public virtual PartEntity PartEntity { get; set; }

    }
}