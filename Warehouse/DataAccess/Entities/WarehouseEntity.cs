﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Entities
{
    public class WarehouseEntity
    {
        public Guid Id { get; set; }
        [Display(Name = "Счет")]
        public string Name { get; set; }
        public Guid? BalanceEntityId { get; set; }
        public Guid? MolEntityId { get; set; }
        public virtual MOLEntity MOL { get; set; }
        public Guid? ContoEntityId { get; set; }
        public virtual ContoEntity Conto { get; set; }
        public virtual BalanceEntity  Balance { get; set; }
    }
}
