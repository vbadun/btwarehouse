﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Entities
{
    public class DocumentTypeEntity
    {
        public Guid Id { get; set; }
        [Display(Name = "Вид документа")]
        public string Name { get; set; }
        public virtual ICollection<DocumentEntity> Documents { get; set; }
    }
}
