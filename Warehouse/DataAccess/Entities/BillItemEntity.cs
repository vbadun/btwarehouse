﻿using System;

namespace DataAccess.Entities
{
    public class BillItemEntity
    {
        public Guid Id { get; set; }
        public double Count { get; set; }
        public DateTime? CreateTime { get; set; } 
        public Guid BillEntityId { get; set; }
        public Guid PartEntityId { get; set; }
        public virtual PartEntity PartEntity { get; set; }
        public virtual BillEntity BillEntity { get; set; }

       
    }
}