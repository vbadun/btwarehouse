﻿using DataAccess.Entities;
using System.Data.Entity.ModelConfiguration;

namespace DataAccess
{
    internal class PartEntityConfiguration : EntityTypeConfiguration<PartEntity>
    {
        public PartEntityConfiguration()
        {
            ToTable("Parts");
        }
    }
}