﻿using DataAccess.Entities;
using System.Data.Entity.ModelConfiguration;

namespace DataAccess
{
    internal class OrderEntityConfiguration : EntityTypeConfiguration<OrderEntity>
    {
            public OrderEntityConfiguration()
            {
                ToTable("Orders");
            }
    }
}