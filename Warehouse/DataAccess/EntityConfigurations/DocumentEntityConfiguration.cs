﻿using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.EntityConfigurations
{
    
    internal class DocumentEntityConfiguration : EntityTypeConfiguration<DocumentEntity>
    {
        public DocumentEntityConfiguration()
        {
            ToTable("Documents");
        }
    }
}
