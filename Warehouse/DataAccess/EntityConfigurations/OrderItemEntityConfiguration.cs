﻿using DataAccess.Entities;
using System.Data.Entity.ModelConfiguration;

namespace DataAccess
{
    internal class OrderItemEntityConfiguration : EntityTypeConfiguration<OrderItemEntity>
    {
        public OrderItemEntityConfiguration()
        {
            ToTable("OrderItems");
        }
    }
}