﻿using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.EntityConfigurations
{
    
    internal class DocumentItemEntityConfiguration : EntityTypeConfiguration<DocumentItemEntity>
    {
        public DocumentItemEntityConfiguration()
        {
            ToTable("DocumentItems");
        }
    }
}
