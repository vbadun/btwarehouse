﻿using Warehouse.Model;
using System;
using System.Collections.Generic;
using System.Text;


namespace Warehouse.Repository
{
    public interface ICustomerRepository : IGenericRepository<Customer>
    {
        Customer GetById(Guid Id);
    }
}
