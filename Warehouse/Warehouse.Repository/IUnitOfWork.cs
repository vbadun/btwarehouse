﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Warehouse.Repository
{
    public interface IUnitOfWork : IDisposable
    {
       int Commit();
    }
}
