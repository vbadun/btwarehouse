﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using Warehouse.Model;

namespace Warehouse.Repository
{
    public class CustomerRepository : GenericRepository<Customer>, ICustomerRepository
    {
        public CustomerRepository(DbContext context) : base(context)
        {

        }
        public Customer GetById(Guid id)
        {
            return FindBy(x => x.Id == id).FirstOrDefault();
        }
    }
}
