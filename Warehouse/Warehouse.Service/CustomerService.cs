﻿using System;
using System.Collections.Generic;
using System.Text;
using Warehouse.Model;
using Warehouse.Repository;

namespace Warehouse.Service
{
    public class CustomerService : EntityService<Customer>,  ICustomerService
    {
        IUnitOfWork _unitOfWork;
        ICustomerRepository _customerRepository;

        public CustomerService(IUnitOfWork unitOfWork, ICustomerRepository customerRepository)
            : base(unitOfWork, customerRepository)
        {
            _unitOfWork = unitOfWork;
            _customerRepository = customerRepository;
        }


        public Customer GetById(Guid Id)
        {
            return _customerRepository.GetById(Id);
        }
    }
}
