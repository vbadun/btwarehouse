﻿using System;
using Warehouse.Model;
using System.Collections.Generic;
using System.Text;

namespace Warehouse.Service
{
    public interface ICustomerService : IEntityService<Customer>
    {
        Customer GetById(Guid Id);
    }
}
