﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Warehouse.Models.Balance
{
    public class BalanceEditModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}