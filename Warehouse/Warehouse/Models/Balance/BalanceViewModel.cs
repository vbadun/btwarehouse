﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Warehouse.Models.Balance
{
    public class BalanceViewModel
    {
        public Guid Id { get; set; }
        [Display(Name = "Счет")]
        public string Name { get; set; }
    }
}