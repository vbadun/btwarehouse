﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Warehouse.Models.Warehouse
{
    public class WarehouseViewModel
    {
        public Guid Id { get; set; }
        [Display(Name = "Склад")]
        public string Name { get; set; }
        [Display(Name = "Счет 1С")]
        public string BalanceName { get; set; }
        [Display(Name = "Субконто")]
        public string ContoName { get; set; }
        [Display(Name = "МОЛ")]
        public string MOLName { get; set; }
    }
}