﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Warehouse.Models.Balance;
using Warehouse.Models.Conto;
using Warehouse.Models.MOL;

namespace Warehouse.Models.Warehouse
{
    public class WarehouseEditModel
    {
        public Guid Id { get; set; }
        [Display(Name = "Имя склада")]
        public string Name { get; set; }
        [Display(Name = "Счет:")]
        public string BalanceName { get; set; }
        public List<BalanceViewModel> BalanceList { get; set; }
        public BalanceViewModel Balance { get; set; }
        [Display(Name = "МОЛ:")]
        public string MOLName { get; set; }
        public List<MOLViewModel> MOLList { get; set; }
        public MOLViewModel MOL { get; set; }
        [Display(Name = "Субконто:")]
        public string ContoName { get; set; }
        public List<ContoViewModel> ContoList { get; set; }
        public ContoViewModel Conto { get; set; }

    }
}