﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Warehouse.Models.DocumentItems
{
    public class DocumentItemsListModel
    {
        public Guid Id { get; set; }
        [Display(Name = "Наименование")]
        public string PartName { get; set; }
        [Display(Name = "Артикул")]
        public string PartNumber { get; set; }
        [Display(Name = "Количество")]
        public decimal Amount { get; set; }
        [Display(Name = "Ед.изм.")]
        public string UnitCount { get; set; }
        [Display(Name = "Счет")]
        public string Warehouse { get; set; }
        [Display(Name = "Субконто")]
        public string Conto { get; set; }
        [Display(Name = "МОЛ")]
        public string Mol { get; set; }
    }
}