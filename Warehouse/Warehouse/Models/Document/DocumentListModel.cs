﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Warehouse.Models.Document
{
    public class DocumentListModel
    {
        public string Id { get; set; }
        [Display(Name = "Наименование")]
        public string Name { get; set; }
        [Display(Name = "Дата документа")]
        public DateTime? DocumentDate { get; set; }
        [Display(Name = "Статус")]
        public string Status { get; set; }

    }
}