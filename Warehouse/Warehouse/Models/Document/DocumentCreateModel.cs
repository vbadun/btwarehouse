﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Warehouse.Models.DocumentItems;
using Warehouse.Models.DocumentType;

namespace Warehouse.Models.Document
{
    public class DocumentCreateModel
    {
        public Guid Id { get; set; }
        [Display(Name = "Тип документа")]
        public DocumentTypeViewModel DocumentType { get; set; }
        [Display(Name = "Имя документа")]
        public string Name { get; set; }
        [Display(Name = "Проведен да/нет")]
        [Required]
        public bool TakeStock { get; set; }
        [Display(Name = "Дата документа")]
        [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd MMMMMMMMMMM yyyy}", ApplyFormatInEditMode = true)]
        public DateTime DocumentDate { get; set; }
        public List<DocumentTypeViewModel> DocumentTypes { get; set; }
    }
}