﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Warehouse.Models.Order
{
    public class PartItem
    {
        public Guid Id { get; set; }
        [Display(Name = "Наименование:")]
        public string PartRusName { get; set; }
        [Display(Name = "EngName:")]
        public int? SerialIndex { get; set; } 
        public string PartEngName { get; set; }
        [Display(Name = "Артикул:")]
        public string PartNumber { get; set; }
        [Display(Name = "Количество:")]
        //[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:F4}")]
        public double Count { get; set; }
        [Display(Name = "Ед.изм")]
        public string UnitCount { get; set; }
    }
}