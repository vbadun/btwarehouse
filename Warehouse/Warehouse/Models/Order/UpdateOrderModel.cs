﻿using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Warehouse.Models.Order
{
    public class UpdateOrderModel
    {
        public Guid OrderId { get; set; }
        public string OrderName { get; set; }
        public IEnumerable<PartItem> PartItems { get; set; }
        public PartItem NewPartItem { get; set; }
    }
}