﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Warehouse.Models.Order
{
    public class AutoOrderCreateModel
    {
        public Guid PartCategoryId { get; set; }
        [Display(Name = "Категория")]
        public List<Models.PartCategories.PartCategory> PartCategoryList { get; set; }
        public PartCategories.PartCategory PartCategory { get; set; }
    }
}