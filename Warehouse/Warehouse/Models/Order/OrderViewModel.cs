﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Warehouse.Models.Order
{
    public class OrderViewModel
    {
        public Guid Id { get; set; }
        [Display(Name = "Наименование")]
        public string Name { get; set; }
        //
        public string Count { get; set; }
        //
        [Display(Name = "Дата заказа")]
        [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd MMMMMMMMMMM yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? OrderDate { get ; set; }
    }
}