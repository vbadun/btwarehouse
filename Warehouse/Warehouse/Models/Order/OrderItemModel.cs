﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Warehouse.Models.Order
{
    public class OrderItemModel
    {
        [Display(Name = "Наименование")]
        public string PartName { get; set; }
        public int? SerialIndex { get; set; }
        [Display(Name = "Артикул")]
        public string PartNumber { get; set; }
        [Display(Name = "Количество")]
        public double Count { get; set; }
        [Display(Name = "Ед.изм.")]
        public string UnitCount { get; set; }
    }
}