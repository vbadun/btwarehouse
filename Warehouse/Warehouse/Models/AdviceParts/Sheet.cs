﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Warehouse.Models.AdviceParts
{
    public class Sheet
    {
        public string SheetName { get; set; }
        public string Path { get; set; }
    }
}