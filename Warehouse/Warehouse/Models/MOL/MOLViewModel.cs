﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Warehouse.Models.MOL
{
    public class MOLViewModel
    {
        public Guid Id { get; set; }
        [Display(Name = "МОЛ")]
        public string Name { get; set; }
    }
}