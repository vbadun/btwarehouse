﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Warehouse.Models.MOL
{
    public class MOLEditModel
    {
        
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}