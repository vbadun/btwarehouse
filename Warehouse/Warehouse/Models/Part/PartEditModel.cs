﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Warehouse.Models.Part
{
    public class PartEditModel
    {
        public Guid? Id { get; set; }
        [Required]
        public string UnitCountId { get; set; }
        [Display(Name = "Единицы измерения")]
        public List<UnitCount> UnitCountList { get; set; }
        public UnitCount UnitCount { get; set; }
        [Display(Name = "Наименование")]
        [Required]
        public string RusName { get; set; }
        [Required]
        [Display(Name = "Наименование англ.")]
        public string EngName { get; set; }
        [Display(Name = "Артикул")]
        [Required]
        public string PartNumber { get; set; }
        [Display(Name = "Мин лимит на складе")]
        [Required]
        public double? LimitQuantity { get; set; }
        //
        [Display(Name = "Категория")]
        [Required]
        public string PartCategoryId { get; set; }
        public List<Models.PartCategories.PartCategory> PartCategoryList { get; set; }
        public PartCategories.PartCategory PartCategory { get; set; }
    }
}