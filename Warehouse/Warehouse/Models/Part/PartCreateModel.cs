﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Warehouse.Models.PartCategory;

namespace Warehouse.Models
{
    public class PartCreateModel
    {
        public string UnitCountId { get; set; }
       
        [Display(Name = "Единицы измерения")]
        public List<UnitCount> UnitCountList { get; set; }
      
        public UnitCount UnitCount { get; set; }
        //
        public string PartCategoryId { get; set; }
       
        [Display(Name = "Категория")]
        public List<Models.PartCategories.PartCategory> PartCategoryList { get; set; }
        
        public PartCategories.PartCategory PartCategory { get; set; }
        //
        [Required]
        [Display(Name = "Наименование")]
        public string RusName { get; set; }
        [Required]
        [Display(Name = "Наименование англ.")]
        public string EngName { get; set; }
        [Required]
        [Display(Name = "Артикул")]
        public string PartNumber { get; set; }
        //LimitCount
        [Required]
        [Display(Name = "Мин лимит на складе")]
        public double? LimitQuantity { get; set; }
    }
}