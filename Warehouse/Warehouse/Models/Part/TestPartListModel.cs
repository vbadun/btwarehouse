﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Warehouse.Models.Part
{
    public class TestPartListModel
    {
            [Display(Name = "Единицы измерения")]
            public string UnitCount { get; set; }
            [Display(Name = "Наименование")]
            public string RusName { get; set; }
            [Display(Name = "НАименование англ.")]
            public string EngName { get; set; }
            [Display(Name = "Артикул")]
            public string PartNumber { get; set; }
    }
}