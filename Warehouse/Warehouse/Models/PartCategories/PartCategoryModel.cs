﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Warehouse.Models.PartCategory
{
    public class PartCategoryModel
    {
        public Guid Id { get; set; }
        [Display(Name = "Категория")]
        public string PartCategoryName { get; set; }
    }
}