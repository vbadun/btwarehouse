﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Warehouse.Models.Conto
{
    public class ContoViewModel
    {
        public Guid Id { get; set; }
        [Display(Name = "Имя субконто")]
        public string Name { get; set; }
    }
}