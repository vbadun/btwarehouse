﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Warehouse.Models.Conto
{
    public class ContoEditModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}