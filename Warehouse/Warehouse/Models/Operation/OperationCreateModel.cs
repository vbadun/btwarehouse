﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Warehouse.Models.Operation
{
    public class OperationCreateModel
    {
        public Guid Id { get; set; }
        public Guid PartId { get; set; }
        [Display(Name = "Наименование")]
        public string PartName { get; set; }
        [Display(Name = "Артикул")]
        public string PartNumber { get; set; }
        [Display(Name = "Количество")]
        public double Quantity { get; set; }
        [Display(Name = "Ед.изм.")]
        public string UnitCount { get; set; }
    }
}