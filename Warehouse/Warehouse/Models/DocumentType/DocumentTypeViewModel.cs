﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Warehouse.Models.DocumentType
{
    public class DocumentTypeViewModel
    {
        public Guid Id { get; set; }
        [Display(Name = "Тип документа")]
        public string Name { get; set; }
    }
}