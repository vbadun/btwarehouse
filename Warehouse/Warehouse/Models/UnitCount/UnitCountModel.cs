﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Warehouse.Models
{
    public class UnitCountModel
    {
        public Guid Id { get; set; }
        [Display(Name = "Ед.изм.")]
        public string UnitCountName { get; set; }
    }
}