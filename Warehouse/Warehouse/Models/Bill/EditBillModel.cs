﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Warehouse.Models.Order;

namespace Warehouse.Models
{
    public class EditBillModel
    {
        //[Display(Name = "Наименование")]
        //public string PartName { get; set; }
        //[Display(Name = "Артикул")]
        //public string PartNumber { get; set; }
        //[Display(Name = "Количество")]
        //public int Count { get; set; }
        public Guid BillId { get; set; }
        public string BillName { get; set; }
      //  public IEnumerable<PartItem> PartItems { get; set; }
        public ICollection<BillItem> BillItems { get; set; }
        public PartItem NewPartItem { get; set; }
    }
}