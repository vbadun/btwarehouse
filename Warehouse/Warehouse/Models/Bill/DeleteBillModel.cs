﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Warehouse.Models
{
    public class DeleteBillModel
    {
        [Display(Name = "Заказчик")]
        public string CustomerName { get; set; }
        [Display(Name = "Дата")]
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}")]
        public DateTime TimeOfCreate { get; set; }
    }
}