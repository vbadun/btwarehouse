using ExelExportService;
using System;
using System.Web.Mvc;
using Unity;
using Unity.Mvc5;

namespace Warehouse
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();
            //      container.RegisterType<IExelExportService, OrderToExelExporter>()
      
            container.RegisterType<ITxtExport, OrderToTxtService>();
            container.RegisterType<IExelExport, OrderToExelExporter>();
            container.RegisterType<IExportFromExel, RestPartsFromExel>();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}