﻿using DataAccess.Entities;
using ExpressMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Warehouse.Models;
using Warehouse.Models.Order;

namespace Warehouse.App_Start
{
    public class MapperConfig
    {
        public static void MappingRegistration()
        {
            Mapper.Register<OrderEntity, OrderViewModel>();
          //  Mapper.Register<OrderItemEntity, OrderItemModel>().Member< OrderItemEntity,OrderItemModel >(dest=>dest.Count,src=>src.Count)
         //   Mapper.Register<OrderItemEntity, OrderItemModel>().Member(dest=>dest.Count, src=>src.)
            //Mapper.Compile();
            Mapper.PrecompileCollection<List<OrderEntity>, List<OrderViewModel>>();
            Mapper.Register<OrderEditModel, OrderEntity>().Ignore(dest => dest.OrderItems);
            Mapper.Compile();

        }
    }
}