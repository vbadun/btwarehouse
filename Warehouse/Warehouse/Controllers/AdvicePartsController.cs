﻿using Aspose.Cells;
using Aspose.Cells.Rendering;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Warehouse.Models.AdviceParts;

namespace Warehouse.Controllers
{
    public class AdvicePartsController : Controller
    {
        public List<Sheet> sheets;

        [HttpGet]
        public ActionResult Index(/*string fileName*/)
        {
            sheets = new List<Sheet>();
            //if (fileName == null)
            //{
            //    // Display default Worksheet on page load
            //   // sheets = RenderExcelWorksheetsAsImage("Текущий заказ Lely.xlsx");
            //}
            //else
            //{
            //  //  sheets = RenderExcelWorksheetsAsImage(fileName);
            //}
            var t = RenderExcelWorksheetsAsImage();

            return View("Index1",t/*sheets*/);
        }



        public /*List<Sheet>*/ List<string> RenderExcelWorksheetsAsImage(/*string FileName*/)
        {

            Workbook partbook = new Workbook(@"D:\1\lely.xlsx");
            Worksheet worksheet = partbook.Worksheets[0];
            ExportTableOptions to = new ExportTableOptions();
            to.PlotVisibleColumns = true;

            int totalRows = worksheet.Cells.MaxRow + 1;
            int totalColumns = worksheet.Cells.MaxColumn;

            var parts = worksheet.Cells.ExportArray(0, 0, totalRows, totalColumns);
            var list = new List<string>();
            var part = "";
            for (int i = 0; i < totalRows; i++)
            {
                if (parts[i, 0] != null)
                {
                    for (int j = totalColumns; j >0;  j--)
                    {
                        part = parts[i, j-1] + "   "+ part;
                    }
                    list.Add(part);
                    part = "";
                }
            }


            // Load the Excel workbook 
         //   Workbook book = new Workbook(Server.MapPath(Path.Combine("~/Content", FileName)));
            var workSheets = new List<Sheet>();
            // Set image rendering options
            ImageOrPrintOptions options = new ImageOrPrintOptions();
            options.HorizontalResolution = 200;
            options.VerticalResolution = 200;
            options.AllColumnsInOnePagePerSheet = true;
            options.OnePagePerSheet = true;
            options.TextCrossType = TextCrossType.Default;
            options.ImageType = Aspose.Cells.Drawing.ImageType.Png;

            string imagePath = "";
            string basePath = Server.MapPath("~/");

            // Create Excel workbook renderer
            //WorkbookRender wr = new WorkbookRender(book, options);
            //// Save and view worksheets
            //for (int j = 0; j < book.Worksheets.Count; j++)
            //{
            //    imagePath = Path.Combine("/Content", string.Format("sheet_{0}.png", j));
            //    wr.ToImage(j, basePath + imagePath);
            //    workSheets.Add(new Sheet { SheetName = string.Format("{0}", book.Worksheets[j].Name), Path = imagePath });
            //}

            return /*workSheets*/ list;
        }
    }
}