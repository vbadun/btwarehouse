﻿using DataAccess;
using DataAccess.Entities;
using Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Warehouse.Models;
using Warehouse.Repository;

namespace Warehouse.Controllers
{
    public class BillController : Controller
    {
        //private UnitOfWork<PartContext> unitOfWork = new UnitOfWork<PartContext>();
        //private GenericRepository<BillEntity> repository;
        //private GenericRepository<CategoryEntity> categoryrepository;
        //private GenericRepository<UnitCountEntity> unitcountrepository;
        //// private GenericRepository<MOLEntity> molrepository;
        ////   private IWarehouseRepository warehouseRepository;
        //public BillController()
        //{
        //    //If you want to use Generic Repository with Unit of work
        //    repository = new GenericRepository<BillEntity>(unitOfWork);
        //    categoryrepository = new GenericRepository<CategoryEntity>(unitOfWork);
        //    unitcountrepository = new GenericRepository<UnitCountEntity>(unitOfWork);
        //    // molrepository = new GenericRepository<MOLEntity>(unitOfWork);
        //    //If you want to use Specific Repository with Unit of work
        //    //   warehouseRepository = new WarehouseRepository(unitOfWork);
        //}


        //************************************************************
        private PartContext db = new PartContext();
        // GET: Bill
        public async Task<ActionResult> Index()
        {
            var bills = await db.Bills.Include(c => c.Customer).ToListAsync();
            var billlist = new List<Bill>();
            foreach (var item in bills)
            {
                billlist.Add(new Bill() { Id = item.Id, Number = item.Number, DateOfCreate = item.DateOfCreate, CustomerName = item.Customer.CustomerName });
            }

            return View(billlist.OrderBy(x => x.Number));
        }

        // GET: Bill/Details/5
        public ActionResult Details(Guid id)
        {
            var bill = db.BillItems.Where(x => x.BillEntityId == id).Include(c => c.PartEntity).ToList();
            var billitemlist = new List<BillItem>();
            int i = 1;

            foreach (var item in bill)
            {

                billitemlist.Add(new BillItem()
                {
                    Count = item.Count.ToString(),
                    Number = i++,
                    CreateTime = item.CreateTime,
                    PartName = item.PartEntity.RusName,
                    PartNumber = item.PartEntity.PartNumber,
                    BillId = id
                });
            }
            return View(billitemlist.OrderBy(x => x.CreateTime));
        }

        public ActionResult CreateBillItem(Guid id)
        {

            return View();
        }

        [HttpPost]
        public ActionResult CreateBillItem(Guid id, BillItem bill)
        {
            var billitem = new BillItem()
            {
                Id = Guid.NewGuid(),
                Count = bill.Count,
                PartNumber = bill.PartNumber,
                CreateTime = DateTime.Now
            };

            var part = db.Parts.Where(x => x.PartNumber == bill.PartNumber).SingleOrDefault();
            //if (bill.Count.Contains(','))
            //{
            //    bill.Count.Replace(',', '.');
            //}
            BillItemEntity bie = new BillItemEntity()
            {
                Id = billitem.Id,
                BillEntityId = id,
                CreateTime = billitem.CreateTime,
                Count = Convert.ToDouble(bill.Count),
                PartEntityId = part.Id
            };
            db.BillItems.Add(bie);
            db.SaveChanges();

            return RedirectToAction("CreateBillItem", bill.Id);
        }


        // GET: Bill/Create
        public ActionResult Create()
        {

            CreateBillModel model = new CreateBillModel();
            return View(model);

        }

        // POST: Bill/Create
        [HttpPost]
        public ActionResult Create(CreateBillModel model)
        {
            try
            {
                BillEntity bi = new BillEntity()
                {
                    Id = Guid.NewGuid(),
                    CustomerId = db.Customers.Where(c => c.CustomerName == model.CustomerName).FirstOrDefault().Id,
                    Number = db.Bills.Select(x => x.Number).ToList().Max() + 1,
                    DateOfCreate = model.TimeOfCreate
                };
                db.Bills.Add(bi);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Bill/Edit/5
        public ActionResult Edit(Guid id)
        {
            var bill = db.Bills.Where(x => x.Id == id).Include(x => x.BillItems).FirstOrDefault();

            var billitems = new List<BillItem>();
            foreach (var item in bill.BillItems)
            {
                var billitem = new BillItem
                {
                    BillId = bill.Id,
                    Count = item.Count.ToString(),
                    CreateTime = item.CreateTime,
                    Id = item.Id,
                    PartName = item.PartEntity.RusName,
                    PartNumber = item.PartEntity.PartNumber
                };
                billitems.Add(billitem);
            }
            EditBillModel billmodel = new EditBillModel() { BillId = id, BillItems = billitems.OrderBy(x => x.CreateTime).ToList() };

            return View("EditBill", billmodel);
        }

        // POST: Bill/Edit/5
        [HttpPost]
        public ActionResult Edit(Guid id, EditBillModel edititem)
        {
            try
            {
                //var billitem = new BillItem()
                //{
                //    Id = Guid.NewGuid(),
                //    Count = edititem.NewPartItem.Count,
                //    PartNumber = bill.PartNumber,
                //    CreateTime = DateTime.Now
                //};

                var part = db.Parts.Where(x => x.PartNumber == edititem.NewPartItem.PartNumber).SingleOrDefault();

                BillItemEntity bie = new BillItemEntity()
                {
                    Id = Guid.NewGuid(),
                    BillEntityId = id,
                    CreateTime = DateTime.Now,
                    Count = Convert.ToDouble(edititem.NewPartItem.Count),
                    PartEntityId = part.Id
                };
                db.BillItems.Add(bie);
                db.SaveChanges();

                return RedirectToAction("Edit", id);
            }
            catch
            {
                return View();
            }
        }

        // GET: Bill/Delete/5
        [Authorize(Roles = "admin")]

        public async Task<ActionResult> Delete(Guid id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                BillEntity bill = await db.Bills.Where(b => b.Id == id).FirstAsync();
                DeleteBillModel delbill = new DeleteBillModel()
                {
                    CustomerName = bill.Customer.CustomerName,
                    TimeOfCreate = bill.DateOfCreate
                };
                if (bill == null)
                {
                    return HttpNotFound();
                }

                return View(delbill);
            }
            catch
            {
                return View();
            }
        }

        // POST: Bill/Delete/5
        [Authorize(Roles = "admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(Guid id)
        {
            BillEntity bill = await db.Bills.FindAsync(id);
            db.Bills.Remove(bill);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

    }
}
