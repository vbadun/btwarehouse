﻿using System;

using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DataAccess;
using DataAccess.Entities;
using Warehouse.Models;
using Warehouse.Models.Part;
using Warehouse.Repository;
using System.Collections;
using System.IO;

namespace Warehouse.Controllers
{
    [Authorize]
    public class SortedPartsController : Controller
    {
       // private PartContext db = new PartContext();

        private UnitOfWork<PartContext> unitOfWork = new UnitOfWork<PartContext>();
        private GenericRepository<PartEntity> repository;
        private GenericRepository<CategoryEntity> categoryrepository;
        private GenericRepository<UnitCountEntity> unitcountrepository;
        // private GenericRepository<MOLEntity> molrepository;
        //   private IWarehouseRepository warehouseRepository;
        public SortedPartsController()
        {
            //If you want to use Generic Repository with Unit of work
            repository = new GenericRepository<PartEntity>(unitOfWork);
            categoryrepository = new GenericRepository<CategoryEntity>(unitOfWork);
            unitcountrepository = new GenericRepository<UnitCountEntity>(unitOfWork);
            // molrepository = new GenericRepository<MOLEntity>(unitOfWork);
            //If you want to use Specific Repository with Unit of work
            //   warehouseRepository = new WarehouseRepository(unitOfWork);
        }
        // GET: Parts

        public ActionResult Index()
        {
            return View("SortedPartsList", repository.GetAll().OrderBy(x => x.RusName));
        }
        // GET: Parts/Details/5
        public /*async Task<ActionResult>*/ ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //  PartEntity part = await db.Parts.FindAsync(id);
            var part = repository.GetById(id);
            if (part == null)
            {
                return HttpNotFound();
            }
            if (part.PictureName == null)
            {
                part.PictureName = "NoImage.jpg";
            }
            return View(part);
        }

        public ActionResult CreateRart()
        {
            var countlistfromdb = unitcountrepository.GetAll();
            var categorylistfromdb = categoryrepository.GetAll();
            var model = new PartCreateModel() { UnitCountList = new List<UnitCount>(), PartCategoryList = new List<Models.PartCategories.PartCategory>() };
            foreach (var item in countlistfromdb)
            {
                var unit = new UnitCount() { Id = item.Id.ToString(), UnitCountName = item.UnitCountName };
                model.UnitCountList.Add(unit);
            }
            foreach (var item in categorylistfromdb)
            {
                var cat = new Models.PartCategories.PartCategory()
                {
                    Id = item.Id.ToString(),
                    PartCategoryName = item.Name
                };
                model.PartCategoryList.Add(cat);
            }

            //var countlistfromdb = db.UnitCounts.ToList();
            //var categorylistfromdb = db.PartCategories.ToList();
            //var model = new PartCreateModel() { UnitCountList = new List<UnitCount>(), PartCategoryList = new List<Models.PartCategories.PartCategory>() };
            //foreach (var item in countlistfromdb)
            //{
            //    var unit = new UnitCount() { Id = item.Id.ToString(), UnitCountName = item.UnitCountName };
            //    model.UnitCountList.Add(unit);
            //}
            //foreach (var item in categorylistfromdb)
            //{
            //    var cat = new Models.PartCategories.PartCategory()
            //    {
            //        Id = item.Id.ToString(),
            //        PartCategoryName = item.Name
            //    };
            //    model.PartCategoryList.Add(cat);
            //}
            return View(model);
        }

        // POST: Parts/CreateRart
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateRart(/*[Bind(Include = "RusName,EngName,PartNumber, UnitCountList")]*/ PartCreateModel part)
        {
            if (ModelState.IsValid)
            {
                PartEntity newpart = new PartEntity()
                {
                    Id = Guid.NewGuid(),
                    EngName = part.EngName.Trim(),
                    PartNumber = part.PartNumber.Trim(),
                    RusName = part.RusName.Trim(),
                    UnitCountId = Guid.Parse(part.UnitCountId),
                    LimitQuantity = part.LimitQuantity,
                    CategoryEntityId = Guid.Parse(part.PartCategoryId)
                };
                 repository.Insert(newpart);// with repository
                //with db.context
                // db.Parts.Add(newpart);
                // await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(part);
        }

        //GET: Parts/Edit/5
        [Authorize]
        public async Task<ActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var countlistfromdb = unitcountrepository.GetAll();
            var categorylistfromdb = categoryrepository.GetAll();
            var model = new PartEditModel() { UnitCountList = new List<UnitCount>(), Id = id, PartCategoryList = new List<Models.PartCategories.PartCategory>() };
            foreach (var item in countlistfromdb)
            {
                var unit = new UnitCount() { Id = item.Id.ToString(), UnitCountName = item.UnitCountName };
                model.UnitCountList.Add(unit);
            }
            foreach (var item in categorylistfromdb)
            {
                var cat = new Models.PartCategories.PartCategory()
                {
                    Id = item.Id.ToString(),
                    PartCategoryName = item.Name
                };
                model.PartCategoryList.Add(cat);
            }
            var part = repository.GetById(id);
            model.EngName = part.EngName;
            model.PartNumber = part.PartNumber;
            model.RusName = part.RusName;
            model.UnitCount = new UnitCount() { UnitCountName = part.UnitCount.UnitCountName, Id = part.UnitCountId.ToString() };
            model.PartCategory = new Models.PartCategories.PartCategory() { PartCategoryName = part.Category.Name, Id = part.Category.Id.ToString() };
            model.LimitQuantity = part.LimitQuantity;
            return View(model);
        }

        // POST: Parts/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(/*[Bind(Include = "Id,RusName,EngName,PartNumber, UnitCountId, PartCategoryId, LimitQuantity")]*/ PartEditModel part)
        {
            //var editpart = repository.GetById(part.Id);
            //editpart.EngName = part.EngName.Trim();
            //editpart.PartNumber = part.PartNumber.Trim();
            //editpart.RusName = part.RusName.Trim();
            //editpart.UnitCountId = Guid.Parse(part.UnitCountId);
            //editpart.LimitQuantity = part.LimitQuantity;
            //editpart.CategoryEntityId = Guid.Parse(part.PartCategoryId);
            if (ModelState.IsValid)
            {
                var editpart = repository.GetById(part.Id);
                editpart.EngName = part.EngName.Trim();
                editpart.PartNumber = part.PartNumber.Trim();
                editpart.RusName = part.RusName.Trim();
                editpart.UnitCountId = Guid.Parse(part.UnitCountId);
                editpart.LimitQuantity = part.LimitQuantity;
                editpart.CategoryEntityId = Guid.Parse(part.PartCategoryId);

                repository.Update(editpart);
                return RedirectToAction("Index");
            }
                    
            return View(part);
        }

        [Authorize]
        // GET: Parts/Delete/5
        public async Task<ActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PartEntity part = repository.GetById(id); //await db.Parts.FindAsync(id);
            if (part == null)
            {
                return HttpNotFound();
            }
            return View(part);
        }
        [Authorize]
        // POST: Parts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(Guid id)
        {
            // PartEntity part = await db.Parts.FindAsync(id);
            PartEntity part = repository.GetById(id);
            repository.Delete(part);
            // db.Parts.Remove(part);
            //await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }
        public ActionResult GetNameSearch(string rusname)
        {
            var name = rusname.ToLower();
           var names = repository.GetAll().Where(x => x.RusName.ToLower().Contains(name));
            //var names = db.Parts.Where(x => x.RusName.ToLower().Contains(rusname)).ToList();
            // return Json(names, JsonRequestBehavior.AllowGet);
            return View("SortedPartsList", names);
        }



        public ActionResult NameToNumberConvert(string term)
        {
            var name = term.ToLower();
            var numbers = repository.GetAll().Where(x => x.RusName == term.ToLower()).Select(x => x.PartNumber).Take(10);
            // var numbers = db.Parts.Where(x => x.RusName == term.ToLower()).Select(x => x.PartNumber).Take(10).ToList();
            return Json(numbers, JsonRequestBehavior.AllowGet);
        }

        public ActionResult NumberToNameConvert(string term)
        {
            var name = term.ToLower();
            var numbers = repository.GetAll().Where(x => x.PartNumber == term.ToLower()).Select(x => x.RusName).Take(10);
            //var numbers = db.Parts.Where(x => x.PartNumber == term.ToLower()).Select(x => x.RusName).Take(10).ToList();
            return Json(numbers, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetNameSearchJson(string term)
        {
            var name = term.ToLower();
            var names = repository.GetAll().Where(x => x.RusName.ToLower().Contains(term)).Select(x => x.RusName).Take(10);
           // var names = db.Parts.Where(x => x.RusName.ToLower().Contains(term)).Select(x => x.RusName).Take(10).ToList();
            return Json(names, JsonRequestBehavior.AllowGet);
        }
                
        public ActionResult GetPartNumberSearch(string partnumber)
        {
            var name = partnumber.ToLower();
            var names = repository.GetAll().Where(x => x.PartNumber.ToLower().Contains(partnumber));
            //var names = db.Parts.Where(x => x.PartNumber.ToLower().Contains(partnumber)).ToList();
            // return Json(names, JsonRequestBehavior.AllowGet);
            return View("SortedPartsList", names);
        }

        public ActionResult GetPartNumberSearchJson(string term)
        {
            var number = term.ToLower();
            var numbers = repository.GetAll().Where(x => x.PartNumber.ToLower().Contains(term)).Select(x => x.PartNumber).Take(10);
           // var numbers = db.Parts.Where(x => x.PartNumber.ToLower().Contains(term)).Select(x => x.PartNumber).Take(10).ToList();
            // return Json(names, JsonRequestBehavior.AllowGet);
            return Json(numbers, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetCategoriesSearchJson(string term)
        {
            var number = term.ToLower();
            var numbers = categoryrepository.GetAll().Where(x => x.Name.ToLower().Contains(term)).Select(x => x.Name).Take(20);
            // var numbers = categoryrepository.GetAll().Where(x => x.Name.ToLower().Contains(term)).Select(x => x.Name).Take(20).ToList();
            return Json(numbers, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetPartOnCategorySearch(string category)
        {
            var cat = category.ToLower();
            var parts = repository.GetAll().Where(x => x.Category.Name.ToLower().Equals(cat)).OrderBy(x => x.RusName).ToList();
            return View("SortedPartsList", parts);
        }

        public ActionResult EngNameToPartConvert(string term)
        {
            var name = term.ToLower();
            var part = repository.GetAll().Where(x => x.EngName == term.ToLower()).Select(x => new { x.RusName, x.PartNumber }).Take(10);
            return Json(part, JsonRequestBehavior.AllowGet);
        }
        
        [HttpPost]
        public ContentResult Upload(HttpPostedFileBase upload, PartEntity pe)
        {
            if (upload != null)
            {
                string fileName = System.IO.Path.GetFileName(upload.FileName);
                string path = "~/content/PartImages/" + fileName;
                upload.SaveAs(Server.MapPath(path));
                //var part = db.Parts.Where(x => x.Id == pe.Id).FirstOrDefault();
                var part = repository.GetById(pe.Id);
                part.PictureName = fileName;
                repository.Update(part);
                //db.Entry(part).State = EntityState.Modified;
                //db.SaveChanges();
                return Content("<h1> Картинка" + pe.PictureName + " добавлена</h1>");
            }
            return Content("<h1> Ошибка !!!</h1>");
        }

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
