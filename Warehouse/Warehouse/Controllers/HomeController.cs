﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Warehouse.Controllers
{
    public class HomeController : Controller
    {
        private readonly PartContext partcontext;
        public HomeController()
        {
            partcontext = new PartContext();
        }
        
        public ActionResult Index()
        {
           //var cat =  partcontext.PartCategories.ToList();
            return View();
        }

        public ActionResult ShowPartList()
        {

            var list = partcontext.Parts.ToList();

            return View(list);
        }
     
        public ActionResult GetNameSearch(string term)
        {
            var name = term.ToLower();
            var names = partcontext.Parts.Where(x => x.RusName.ToLower().Contains(term)).ToList();

            // return Json(names, JsonRequestBehavior.AllowGet);
            return View("ShowPartList", names);
        }
       

        //public ActionResult GetPartSearch(string rusname, string engname, string partnumber)
        //{

        //    partnumber.ToLower();
        //    var partnumbers = partcontext.Parts.Where(x => x.PartNumber.ToLower().Contains(partnumber));
        //    var rusnames =  partcontext.Parts.Where(x => x.RusName.ToLower().Contains(rusname));
        //    var engnames = partcontext.Parts.Where(x => x.EngName.ToLower().Contains(engname));
        //    var names = rusnames.Union(partnumbers).Union(engnames).ToList();

        //    // return Json(names, JsonRequestBehavior.AllowGet);
        //    return View("ShowPartList", names);
        //}
        


        public ActionResult About()
        {
            OrderManager om = new OrderManager();
            om.CreateOrder();

            return View();
        }

        

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}