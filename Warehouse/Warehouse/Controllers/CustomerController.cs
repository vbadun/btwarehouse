﻿using Aspose.Cells;
using DataAccess;
using DataAccess.Entities;
using Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;


namespace Warehouse.Controllers
{
    public class CustomerController : Controller
    {
        private PartContext db = new PartContext();
    

        // GET: Custome
        public CustomerController()
        {

        }

        public async Task<ActionResult> Index()
        {
                      
            var customers = await db.Customers.OrderBy(x => x.CustomerName).ToListAsync();
            var custlist = new List<Customer>();
            foreach (var item in customers)
            {
                custlist.Add(new Customer() { Id = item.Id, CustomerName = item.CustomerName });
            }
           
            return View(custlist);
        }



        // GET: Custome/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Custome/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Custome/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "CustomerName")]Customer customer)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    customer.Id = Guid.NewGuid();

                    var cust = new CustomerEntity() {
                     Id = customer.Id, CustomerName = customer.CustomerName};
                    
                    db.Customers.Add(cust);
                    await db.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
                else
                {
                    return View();
                }

            }
            catch
            {
                return View();
            }
          
        }



        // GET: Custome/Edit/5
        public ActionResult Edit(Guid id)
        {
            var cust = db.Customers.Where(x => x.Id == id).FirstOrDefault();
            var editcust = new Customer() { Id = id, CustomerName = cust.CustomerName };
            return View(editcust);
        }

        // POST: Custome/Edit/5
        [HttpPost]
        public async Task<ActionResult> Edit(Guid id, [Bind(Include = "CustomerName")]Customer customer)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var cust = new CustomerEntity() { Id = id, CustomerName = customer.CustomerName };
                    db.Entry(cust).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
                return RedirectToAction("Index");
           
            }
            catch
            {
                return View();
            }
        }

        // GET: Custome/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Custome/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        public ActionResult GetCustomerSearchJson(string term)
        {
            var customer = term.ToLower();
            var customers = db.Customers.Where(x => x.CustomerName.ToLower().Contains(term)).Select(x => x.CustomerName).Take(10).ToList();
            return Json(customers, JsonRequestBehavior.AllowGet);
        }

    }
}
