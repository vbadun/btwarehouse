﻿using DataAccess;
using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Warehouse.Models.Part;
using Warehouse.Repository;

namespace Warehouse.Controllers
{
    public class TestPartController : Controller
    {
        private UnitOfWork<PartContext> unitOfWork = new UnitOfWork<PartContext>();
        private GenericRepository<PartEntity> repository;
        private IPartRepository partRepository;
        public TestPartController()
        {
            //If you want to use Generic Repository with Unit of work
            repository = new GenericRepository<PartEntity>(unitOfWork);
            //If you want to use Specific Repository with Unit of work
            partRepository = new PartRepository(unitOfWork);
        }

        // GET: TestRart
        public ActionResult Index()
        {
            var parts = partRepository.GetAll();
            var list = new List<TestPartListModel>();
            foreach (var part in parts)
            {
                list.Add(new TestPartListModel()
                {
                    EngName = part.EngName,
                    PartNumber = part.PartNumber,
                    RusName = part.RusName,
                    UnitCount = part.UnitCount.UnitCountName
                });
            }
            return View(list);
        }

        public ActionResult GetNameSearchJson(string term)
        {
            var names = partRepository.GetPartsByName(term).Select(x => x.RusName).Take(15).ToList();
            return Json(names, JsonRequestBehavior.AllowGet);
        }


    }
}