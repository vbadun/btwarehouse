﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Warehouse.Models.Balance;
using System.Data.Entity;
using Warehouse.Repository;
using DataAccess.Entities;

namespace Warehouse.Controllers
{
    public class BalanceController : Controller
    {
        private UnitOfWork<PartContext> unitOfWork = new UnitOfWork<PartContext>();
        private GenericRepository<BalanceEntity> repository;
        //private IPartRepository partRepository;
        public BalanceController()
        {
            //If you want to use Generic Repository with Unit of work
            repository = new GenericRepository<BalanceEntity>(unitOfWork);
            //If you want to use Specific Repository with Unit of work
            // partRepository = new PartRepository(unitOfWork);
        }
        // GET: Conto
        public ActionResult Index()
        {
            var list = new List<BalanceViewModel>();
            var bals = repository.GetAll();
            foreach (var item in bals)
            {
                var bal = new BalanceViewModel()
                {
                    Id = item.Id,
                    Name = item.Name
                };
                list.Add(bal);
            }
            return View(list);
        }

        // GET: MOL/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: MOL/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: MOL/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here
                var bal = new BalanceEntity() { Id = Guid.NewGuid() };

                UpdateModel(bal, collection);
                unitOfWork.CreateTransaction();
                if (ModelState.IsValid)
                {
                    repository.Insert(bal);
                    unitOfWork.Save();
                    unitOfWork.Commit();
                    return RedirectToAction("Index");
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: MOL/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: MOL/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: MOL/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: MOL/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
