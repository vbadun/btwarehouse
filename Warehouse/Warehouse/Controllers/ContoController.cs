﻿using DataAccess;
using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Warehouse.Models.Conto;
using Warehouse.Repository;


namespace Warehouse.Controllers
{
    public class ContoController : Controller
    {
        private UnitOfWork<PartContext> unitOfWork = new UnitOfWork<PartContext>();
        private GenericRepository<ContoEntity> repository;
        //private IPartRepository partRepository;
        public ContoController()
        {
            //If you want to use Generic Repository with Unit of work
            repository = new GenericRepository<ContoEntity>(unitOfWork);
            //If you want to use Specific Repository with Unit of work
           // partRepository = new PartRepository(unitOfWork);
        }
        // GET: Conto
        public ActionResult Index()
        {
            var list = new List<ContoViewModel>();
            var contos = repository.GetAll();
            foreach (var item in contos)
            {
                var conto = new ContoViewModel()
                {
                    Id = item.Id,
                     Name = item.Name
                };
                list.Add(conto);
            }

            return View(list);
        }

        // GET: Conto/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Conto/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Conto/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
         
            try
            {
                // TODO: Add insert logic here
                var conto = new ContoEntity() {Id = Guid.NewGuid() };
              
                     UpdateModel(conto, collection);
                unitOfWork.CreateTransaction();
                if (ModelState.IsValid)
                {
                    repository.Insert(conto);
                    unitOfWork.Save();
                    unitOfWork.Commit();
                    return RedirectToAction("Index");
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Conto/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Conto/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Conto/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Conto/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
