﻿using DataAccess;
using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Warehouse.Repository;
using Warehouse.Models.MOL;

namespace Warehouse.Controllers
{
    public class MOLController : Controller
    {
        private UnitOfWork<PartContext> unitOfWork = new UnitOfWork<PartContext>();
        private GenericRepository<MOLEntity> repository;
        //private IPartRepository partRepository;
        public MOLController()
        {
            //If you want to use Generic Repository with Unit of work
            repository = new GenericRepository<MOLEntity>(unitOfWork);
            //If you want to use Specific Repository with Unit of work
            // partRepository = new PartRepository(unitOfWork);
        }
        // GET: Conto
        public ActionResult Index()
        {
            var list = new List<MOLViewModel>();
            var mols = repository.GetAll();
            foreach (var item in mols)
            {
                var mol = new MOLViewModel()
                {
                    Id = item.Id,
                    Name = item.Name
                };
                list.Add(mol);
            }
            return View(list);
        }

        // GET: MOL/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: MOL/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: MOL/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here
                var mol = new MOLEntity() { Id = Guid.NewGuid() };

                UpdateModel(mol, collection);
                unitOfWork.CreateTransaction();
                if (ModelState.IsValid)
                {
                    repository.Insert(mol);
                    unitOfWork.Save();
                    unitOfWork.Commit();
                    return RedirectToAction("Index");
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: MOL/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: MOL/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: MOL/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: MOL/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
