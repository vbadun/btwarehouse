﻿using DataAccess;
using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Warehouse.Models.DocumentItems;
using Warehouse.Repository;

namespace Warehouse.Controllers
{
    public class DocumentItemsController : Controller
    {
        private UnitOfWork<PartContext> unitOfWork = new UnitOfWork<PartContext>();
        private GenericRepository<DocumentItemEntity> repository;
        // private IPartRepository partRepository;
        public DocumentItemsController()
        {
            //If you want to use Generic Repository with Unit of work
            repository = new GenericRepository<DocumentItemEntity>(unitOfWork);
            //If you want to use Specific Repository with Unit of work
            // partRepository = new PartRepository(unitOfWork);
        }

        // GET: Document
        public ActionResult Document(Guid id)
        {
            id = Guid.Parse("4acaa671-0ff0-4785-959b-1cf73c4632c0");
            var docs = repository.GetAll().Where(x=>x.Document.Id ==id).ToList();
            var list = new List<DocumentItemsListModel>();
            //foreach (var doc in docs)
            //{
            //    var item = new DocumentItemsListModel()
            //    {

            //    };

            //    list.Add(item);
            //}
            return View(list);
        }
    }
}