﻿using DataAccess;
using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Warehouse.Models.Document;
using Warehouse.Models.DocumentItems;
using Warehouse.Models.DocumentType;
using Warehouse.Repository;

namespace Warehouse.Controllers
{
    public class DocumentController : Controller
    {
        private UnitOfWork<PartContext> unitOfWork = new UnitOfWork<PartContext>();
        private GenericRepository<DocumentEntity> repository;
        private GenericRepository<DocumentTypeEntity> doctyperepository;
        private IDocumentRepository docRepository;
        public DocumentController()
        {
            //If you want to use Generic Repository with Unit of work
            repository = new GenericRepository<DocumentEntity>(unitOfWork);
            doctyperepository = new GenericRepository<DocumentTypeEntity>(unitOfWork);
            //If you want to use Specific Repository with Unit of work
            docRepository = new DocumentRepository(unitOfWork);
        }

        // GET: Document
        public ActionResult Index()
        {
            var docs = repository.GetAll();
            var list = new List<DocumentListModel>();
            foreach (var doc in docs)
            {
                var item = new DocumentListModel()
                {
                    Id = doc.Id.ToString(),
                    Name = doc.Name,
                    DocumentDate = doc.DocumentDate,

                };
                if (doc.TakeStock == null || doc.TakeStock == false)
                {
                    item.Status = "Не проведен";
                }
                else item.Status = "Проведен";
                list.Add(item);
            }
            return View(list);
        }
        public ActionResult DocTypesList(Guid id)
        {
            var docs = repository.GetAll().Where(x => x.DocumentTypeEntityId == id);
            var list = new List<DocumentListModel>();
            foreach (var doc in docs)
            {
                var item = new DocumentListModel()
                {
                    Id = doc.Id.ToString(),
                    Name = doc.Name,
                    DocumentDate = doc.DocumentDate,

                };
                if (doc.TakeStock == null || doc.TakeStock == false)
                {
                    item.Status = "Не проведен";
                }
                else item.Status = "Проведен";
                list.Add(item);
            }
            return View("Index", list);
        }
        // GET: Document
        public ActionResult Document(Guid id)
        {
            var docs = repository.GetById(id);
            var list = new List<DocumentItemsListModel>();
            foreach (var doc in docs.DocumentItems)
            {
                var item = new DocumentItemsListModel()
                {
                    Id = doc.Id,
                    Amount = doc.Amount,
                    UnitCount = doc.Part.UnitCount.UnitCountName,
                    Conto = doc.WarehouseReciever.Conto.Name,
                    PartName = doc.Part.RusName,
                    PartNumber = doc.Part.PartNumber,
                    Mol = doc.WarehouseReciever.MOL.Name,
                    Warehouse = doc.WarehouseReciever.Balance.Name
                };

                list.Add(item);
            }
            return View(list);
        }
        public ActionResult Details(Guid id)
        {
            return RedirectToAction("Index", "DocumentItems", id.ToString());
        }
        // GET: Doc/Create
        public ActionResult Create()
        {
            var doctypes = doctyperepository.GetAll().Select(x => new DocumentTypeViewModel() { Id = x.Id, Name = x.Name }).ToList();
            var model = new DocumentCreateModel()
            {
                DocumentTypes = doctypes
            };
            return View(model);
        }

        // POST: Doc/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                var doc = new DocumentEntity()
                {
                    Id = Guid.NewGuid(),
                    DocumentTypeEntityId = Guid.Parse(collection["DocumentType.Id"]),
                    Name = collection["Name"],
                    DocumentDate = DateTime.Parse(collection["DocumentDate"]),
                    TakeStock = bool.Parse(collection["TakeStock"])
                };
                unitOfWork.CreateTransaction();
                if (ModelState.IsValid)
                {
                    repository.Insert(doc);
                    unitOfWork.Save();
                    unitOfWork.Commit();
                    return RedirectToAction("Index");
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Conto/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

    }
}