﻿using DataAccess;
using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Warehouse.Models.Operation;

namespace Warehouse.Controllers
{
    public class OperationsController : Controller
    {
        private PartContext db = new PartContext();

        // GET: Operations
        public ActionResult Index()
        {

            return View();
        }

        // GET: Operations/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Operations/Create
        public ActionResult Create()
        {
            var operationmodel = new OperationCreateModel() { };
            return View(operationmodel);
        }

        // POST: Operations/Create
        [HttpPost]
        public ActionResult Create(OperationCreateModel operation)
        {
            try
            {
                var operentity = new OperationEntity()
                {
                    Id = Guid.NewGuid(),
                    PartEntityId = db.Parts.Where(x => x.PartNumber == operation.PartNumber).Select(x => x.Id).First(),
                    Quantity = operation.Quantity
                };
                db.Operations.Add(operentity);
                db.SaveChanges();
                // TODO: Add insert logic here

                return RedirectToAction("Create","Operations");
            }
            catch
            {
                return View();
            }
        }

        // GET: Operations/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Operations/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Operations/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Operations/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
