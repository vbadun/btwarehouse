﻿using DataAccess;
using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Warehouse.Repository;

namespace Warehouse.Controllers
{
    public class PartSearchesController : ApiController
    {
        private UnitOfWork<PartContext> unitOfWork = new UnitOfWork<PartContext>();
        private GenericRepository<PartEntity> repository;
        private GenericRepository<CategoryEntity> categoryrepository;
        private GenericRepository<UnitCountEntity> unitcountrepository;
        // private GenericRepository<MOLEntity> molrepository;
        //   private IWarehouseRepository warehouseRepository;
        public PartSearchesController()
        {
            //If you want to use Generic Repository with Unit of work
            repository = new GenericRepository<PartEntity>(unitOfWork);
            categoryrepository = new GenericRepository<CategoryEntity>(unitOfWork);
            unitcountrepository = new GenericRepository<UnitCountEntity>(unitOfWork);
            // molrepository = new GenericRepository<MOLEntity>(unitOfWork);
            //If you want to use Specific Repository with Unit of work
            //   warehouseRepository = new WarehouseRepository(unitOfWork);
        }

        public IEnumerable<string> Get()
        {
            return repository.GetAll().Select(x=>x.RusName);
        }

        //public ActionResult NameToNumberConvert(string term)
        //{
        //    var name = term.ToLower();
        //    var numbers = repository.GetAll().Where(x => x.RusName == term.ToLower()).Select(x => x.PartNumber).Take(10);
        //    // var numbers = db.Parts.Where(x => x.RusName == term.ToLower()).Select(x => x.PartNumber).Take(10).ToList();
        //    return Json(numbers, JsonRequestBehavior.AllowGet);
        //}




        //// GET: api/Values
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        //// GET: api/Values/5
        //public string Get(int id)
        //{
        //    return "value";
        //}

        //// POST: api/Values
        //public void Post([FromBody]string value)
        //{
        //}

        //// PUT: api/Values/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE: api/Values/5
        //public void Delete(int id)
        //{
        //}
    }
}
