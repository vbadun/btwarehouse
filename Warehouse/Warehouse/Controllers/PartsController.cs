﻿using System;

using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DataAccess;
using DataAccess.Entities;
using Warehouse.Models;
using Warehouse.Models.Part;

namespace Warehouse.Controllers
{
    [Authorize]
    public class PartsController : Controller
    {
        private PartContext db = new PartContext();

        // GET: Parts

        public async Task<ActionResult> Index()
        {

            return View("PartsList", await db.Parts.OrderBy(x => x.RusName).ToListAsync());
        }

        //public async Task<ActionResult> PictureRename()
        //{
        //    var parts = db.Parts.ToList();
        //    foreach (var item in parts)
        //    {
        //        if (item.PictureName == null)
        //        {
        //            parts.Remove(item);
        //        }
        //    }


        //    return View("PartsList", await db.Parts.OrderBy(x => x.RusName).ToListAsync());
        //}


        // GET: Parts/Details/5
        public async Task<ActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PartEntity part = await db.Parts.FindAsync(id);
            if (part == null)
            {
                return HttpNotFound();
            }
            if (part.PictureName == null)
            {
                part.PictureName = "NoImage.jpg";
            }
            return View(part);
        }


        public ActionResult CreateRart()
        {
            var countlistfromdb = db.UnitCounts.ToList();
            var categorylistfromdb = db.PartCategories.ToList();

            var model = new PartCreateModel() { UnitCountList = new List<UnitCount>(), PartCategoryList = new List<Models.PartCategories.PartCategory>() };
            foreach (var item in countlistfromdb)
            {
                var unit = new UnitCount() { Id = item.Id.ToString(), UnitCountName = item.UnitCountName };
                model.UnitCountList.Add(unit);
            }
            //
            foreach (var item in categorylistfromdb)
            {
                var cat = new Models.PartCategories.PartCategory()
                {
                    Id = item.Id.ToString(),
                    PartCategoryName = item.Name
                };
                model.PartCategoryList.Add(cat);
            }
            //
            return View(model);
        }

        // POST: Parts/CreateRart
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateRart(/*[Bind(Include = "RusName,EngName,PartNumber, UnitCountList")]*/ PartCreateModel part)
        {
            if (ModelState.IsValid)
            {
                PartEntity newpart = new PartEntity()
                {
                    Id = Guid.NewGuid(),
                    EngName = part.EngName.Trim(),
                    PartNumber = part.PartNumber.Trim(),
                    RusName = part.RusName.Trim(),
                    UnitCountId = Guid.Parse(part.UnitCountId),
                    LimitQuantity = part.LimitQuantity,
                    CategoryEntityId = Guid.Parse(part.PartCategoryId)

                };
                db.Parts.Add(newpart);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(part);
        }

        //GET: Parts/Edit/5
        [Authorize]
        public async Task<ActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var countlistfromdb = await db.UnitCounts.ToListAsync();
            var categorylistfromdb = db.PartCategories.ToList();

            var model = new PartEditModel() { UnitCountList = new List<UnitCount>(), Id = id, PartCategoryList = new List<Models.PartCategories.PartCategory>() };
            foreach (var item in countlistfromdb)
            {
                var unit = new UnitCount() { Id = item.Id.ToString(), UnitCountName = item.UnitCountName };
                model.UnitCountList.Add(unit);
            }
            //
            foreach (var item in categorylistfromdb)
            {
                var cat = new Models.PartCategories.PartCategory()
                {
                    Id = item.Id.ToString(),
                    PartCategoryName = item.Name
                };
                model.PartCategoryList.Add(cat);
            }

            var part = await db.Parts.FindAsync(id);
            model.EngName = part.EngName;
            model.PartNumber = part.PartNumber;
            model.RusName = part.RusName;
            model.UnitCount = new UnitCount() { UnitCountName = part.UnitCount.UnitCountName, Id = part.UnitCountId.ToString() };
            model.PartCategory = new Models.PartCategories.PartCategory() { PartCategoryName = part.Category.Name, Id = part.Category.Id.ToString() };
            model.LimitQuantity = part.LimitQuantity;
            return View(model);
        }

        // POST: Parts/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(/*[Bind(Include = "Id,RusName,EngName,PartNumber, UnitCountId, PartCategoryId, LimitQuantity")]*/ PartEditModel part)
        {
            var editpart = db.Parts.Find(part.Id);
            editpart.EngName = part.EngName.Trim();
            editpart.PartNumber = part.PartNumber.Trim();
            editpart.RusName = part.RusName.Trim();
            editpart.UnitCountId = Guid.Parse(part.UnitCountId);
            editpart.LimitQuantity = part.LimitQuantity;
            editpart.CategoryEntityId = Guid.Parse(part.PartCategoryId);
            if (ModelState.IsValid)
            {
                db.Entry(editpart).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(part);
        }


        [Authorize]
        // GET: Parts/Delete/5
        public async Task<ActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PartEntity part = await db.Parts.FindAsync(id);
            if (part == null)
            {
                return HttpNotFound();
            }
            return View(part);
        }
        [Authorize]
        // POST: Parts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(Guid id)
        {
            PartEntity part = await db.Parts.FindAsync(id);
            db.Parts.Remove(part);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }
        public ActionResult GetNameSearch(string rusname)
        {
            var name = rusname.ToLower();
            var names = db.Parts.Where(x => x.RusName.ToLower().Contains(rusname)).ToList();

            // return Json(names, JsonRequestBehavior.AllowGet);
            return View("PartsList", names);
        }



        public ActionResult NameToNumberConvert(string term)
        {
            var name = term.ToLower();
            var numbers = db.Parts.Where(x => x.RusName == term.ToLower()).Select(x => x.PartNumber).Take(10).ToList();
            return Json(numbers, JsonRequestBehavior.AllowGet);
        }

        public ActionResult NumberToNameConvert(string term)
        {
            var name = term.ToLower();
            var numbers = db.Parts.Where(x => x.PartNumber == term.ToLower()).Select(x => x.RusName).Take(10).ToList();
            return Json(numbers, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetNameSearchJson(string term)
        {
            var name = term.ToLower();
            var names = db.Parts.Where(x => x.RusName.ToLower().Contains(term)).Select(x => x.RusName).Take(10).ToList();
            return Json(names, JsonRequestBehavior.AllowGet);
        }



        public ActionResult GetPartNumberSearch(string partnumber)
        {
            var name = partnumber.ToLower();
            var names = db.Parts.Where(x => x.PartNumber.ToLower().Contains(partnumber)).ToList();

            // return Json(names, JsonRequestBehavior.AllowGet);
            return View("PartsList", names);
        }

        public ActionResult GetPartNumberSearchJson(string term)
        {
            var number = term.ToLower();
            var numbers = db.Parts.Where(x => x.PartNumber.ToLower().Contains(term)).Select(x => x.PartNumber).Take(10).ToList();

            // return Json(names, JsonRequestBehavior.AllowGet);
            return Json(numbers, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EngNameToPartConvert(string term)
        {

            var name = term.ToLower();
            var part = db.Parts.Where(x => x.EngName == term.ToLower()).Select(x => new { x.RusName, x.PartNumber }).Take(10).ToList();
            return Json(part, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ContentResult Upload(HttpPostedFileBase upload, PartEntity pe)
        {
            if (upload != null)
            {
                string fileName = System.IO.Path.GetFileName(upload.FileName);
                //string path = "~/content/PartImages/" + fileName;
                string newpath;
                var part = db.Parts.Where(x => x.Id == pe.Id).FirstOrDefault();
                if (part.PartNumber != null)
                {
                     newpath = "~/content/PartImages/" + part.PartNumber + "." + fileName.Split('.').Last();
                }
                else
                {
                     newpath = "~/content/PartImages/" + Guid.NewGuid().ToString() + "." + fileName.Split('.').Last();
                }
                //upload.SaveAs(Server.MapPath(path));
                //part.PictureName = fileName;
                upload.SaveAs(Server.MapPath(newpath));
                part.PictureName = newpath.Split('/').Last();
                db.Entry(part).State = EntityState.Modified;
                db.SaveChanges();
                return Content("<h1> Картинка" + pe.PictureName + " добавлена</h1>");
            }
            return Content("<h1> Ошибка !!!</h1>");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
