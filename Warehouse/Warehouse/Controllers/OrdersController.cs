﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DataAccess;
using DataAccess.Entities;
using Warehouse.Models;
using ExelExportService;
using System.IO;
using Warehouse.Models.Order;
using ExpressMapper;

namespace Warehouse.Controllers
{
    [Authorize]
    public class OrdersController : Controller
    {

        private PartContext db = new PartContext();
        private readonly IExelExport exelexporter;
        private readonly ITxtExport txtexporter;
        private readonly IExportFromExel fromexelexporter;
        public OrdersController(IExelExport _exelexporter, ITxtExport _txtexporter, IExportFromExel _fromexelexporter)
        {
            exelexporter = _exelexporter;
            txtexporter = _txtexporter;
            fromexelexporter = _fromexelexporter;
        }

        // GET: Orders
        public async Task<ActionResult> Index()
        {
            var parts = await db.Orders.ToListAsync();
            var model = Mapper.Map<List<OrderEntity>, List<OrderViewModel>>(parts).OrderBy(x => x.OrderDate);
            return View(model);
        }
        [HttpPost]
        public async Task<ActionResult> Index(string count)
        {
            var parts = await db.Orders.ToListAsync();
            int number;
            bool isParsable = Int32.TryParse(count, out number);
            if (!isParsable) number = parts.Count();
            var model = Mapper.Map<List<OrderEntity>, List<OrderViewModel>>(parts).OrderBy(x => x.OrderDate).Skip(parts.Count - number);
            return View(model);
        }

        // GET: Orders/Details/5
        public async Task<ActionResult> Details(Guid id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var orderlist = await db.OrderItems.Include(x => x.PartEntity).
                Where(x => x.OrderEntityId == id).ToListAsync();
            var order = db.Orders.Find(id);

            var oi = new List<OrderItemModel>();
            foreach (var item in orderlist)
            {
                var p = new OrderItemModel()
                {
                    Count = item.Count,
                    PartName = item.PartEntity.RusName,
                    PartNumber = item.PartEntity.PartNumber,
                    UnitCount = item.PartEntity.UnitCount.UnitCountName,
                    SerialIndex = item.SerialIndex
                };
                oi.Add(p);
            }
            var model = new OrderDetailsModel()
            {
                Id = order.Id,
                Name = order.Name,
                OrderDate = order.OrderDate,
                OrderItems = oi.OrderBy(x => x.SerialIndex).ToList()
                // new List<OrderItemModel>() { new OrderItemModel() { Count = 2, PartName = "part1", PartNumber = "222", UnitCount = "liters" } }
            };//Mapper.Map<List<OrderItemEntity>,List<OrderItemModel>>(orderlist) };
            if (order == null)
            {
                return HttpNotFound();
            }
            return View("OrderDetails", model);
        }

        // GET: Orders/Create
        public ActionResult Create()
        {
            return View("CreateOrder");
        }

        // POST: Orders/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Name, OrderDate")] OrderCreateModel order)
        {
            if (ModelState.IsValid)
            {
                order.Id = Guid.NewGuid();
                if (order.OrderDate == null)
                {
                    order.OrderDate = DateTime.Now;
                }
                var orderEntity = new OrderEntity()
                {
                    Id = order.Id,
                    Name = order.Name,
                    OrderDate = order.OrderDate,
                };
                db.Orders.Add(orderEntity);
                await db.SaveChangesAsync();
                return RedirectToAction("Details", new { id = order.Id.ToString() });
            }
            return View(order);
        }

        // GET: Orders/Edit/5
        public async Task<ActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var order = await db.Orders.FindAsync(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            // var model = Mapper.Map<OrderEntity, OrderEditModel>(order);
            var editmodel = new OrderEditModel() { Id = order.Id, Name = order.Name, OrderDate = order.OrderDate };
            return View("OrderEdit", editmodel);
        }

        // POST: Orders/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name,OrderDate")] OrderEditModel order)
        {
            var orderdb = Mapper.Map<OrderEditModel, OrderEntity>(order);
            if (ModelState.IsValid)
            {
                db.Entry(orderdb).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(order);
        }

        // GET: Orders/Delete/5
        [Authorize(Roles = "admin")]
        public async Task<ActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrderEntity orderEntity = await db.Orders.FindAsync(id);
            if (orderEntity == null)
            {
                return HttpNotFound();
            }
            return View(orderEntity);
        }

        // POST: Orders/Delete/5
        [Authorize(Roles = "admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(Guid id)
        {
            OrderEntity orderEntity = await db.Orders.FindAsync(id);
            db.Orders.Remove(orderEntity);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }
        // GET: Orders/EditOrderItem/5
        public async Task<ActionResult> EditOrderItem(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrderItemEntity orderItemEntity = await db.OrderItems.FindAsync(id);
            if (orderItemEntity == null)
            {
                return HttpNotFound();
            }
            var orderitem = new OrderItemEditModel()
            {
                Count = orderItemEntity.Count,
                PartName = orderItemEntity.PartEntity.RusName,
                PartNumber = orderItemEntity.PartEntity.PartNumber,
                UnitCount = orderItemEntity.PartEntity.UnitCount.UnitCountName
            };
            return View(orderitem);
        }

        // POST: Orders/EditOrderItem/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditOrderItem(OrderItemModel orderItem, Guid? id)
        {
            OrderItemEntity orderItemEntity = await db.OrderItems.FindAsync(id);
            if (orderItemEntity == null)
            {
                return HttpNotFound();
            }
            orderItemEntity.Count = orderItem.Count;
            db.Entry(orderItemEntity).State = EntityState.Modified;
            await db.SaveChangesAsync();
            return RedirectToAction("UpdateOrder", "Orders", new { id = orderItemEntity.OrderEntityId.ToString() });
        }
        // GET: Orders/Delete/5
        [Authorize(Roles = "admin")]
        public async Task<ActionResult> DeleteOrderItem(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrderItemEntity item = await db.OrderItems.FindAsync(id);
            if (item == null)
            {
                return HttpNotFound();
            }
            return View(item);
        }
        // POST: OrderItems/Delete/5
        [Authorize(Roles = "admin")]
        [HttpPost, ActionName("DeleteOrderItem")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteOrderItemConfirmed(Guid id)
        {
            OrderItemEntity item = await db.OrderItems.FindAsync(id);
            db.OrderItems.Remove(item);
            await db.SaveChangesAsync();
            return RedirectToAction("UpdateOrder", "Orders", new { id = item.OrderEntityId.ToString() });
        }
        // GET: Orders/EditOrder/5
        public async Task<ActionResult> UpdateOrder(Guid id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var order = await db.Orders.Where(x => x.Id == id).SingleOrDefaultAsync();
            var parts = new List<PartItem>();
            foreach (var item in order.OrderItems)
            {
                var part = new PartItem()
                {
                    Id = item.Id,
                    Count = item.Count,
                    PartEngName = item.PartEntity.EngName,
                    PartNumber = item.PartEntity.PartNumber,
                    PartRusName = item.PartEntity.RusName,
                    UnitCount = item.PartEntity.UnitCount.UnitCountName,
                    SerialIndex = item.SerialIndex
                };
                parts.Add(part);
            }
            var model = new UpdateOrderModel()
            {
                OrderId = id,
                OrderName = order.Name,
                NewPartItem = new PartItem() { SerialIndex = parts.Count + 1 },
                PartItems = parts.OrderBy(x => x.SerialIndex) //Mapper.Map<List<OrderItemEntity>, List<PartItem>>(order.OrderItems.ToList())
            };
            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }


        // POST: Orders/EditOrderItem/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> UpdateOrder([Bind(Include = "OrderId,NewPartItem")] UpdateOrderModel ordermodel)
        {
            if (ordermodel.NewPartItem == null)
            {
                return HttpNotFound();
            }
            Guid PartId;
            if (ordermodel.NewPartItem.PartNumber == null)
            {
                PartId = db.Parts.Where(x => x.RusName == ordermodel.NewPartItem.PartRusName).SingleOrDefault().Id;
            }
            else
            {
                PartId = db.Parts.Where(x => x.PartNumber == ordermodel.NewPartItem.PartNumber).SingleOrDefault().Id;
            }
            OrderItemEntity orderItemEntity = new OrderItemEntity()
            {
                Id = Guid.NewGuid(),
                OrderEntityId = ordermodel.OrderId,
                PartEntityId = PartId,
                Count = ordermodel.NewPartItem.Count,
                SerialIndex = ordermodel.NewPartItem.SerialIndex
            };
            db.OrderItems.Add(orderItemEntity);
            await db.SaveChangesAsync();
            return RedirectToAction("UpdateOrder", ordermodel.OrderId);
        }

        public FileResult ExportOrder(Guid id)
        {
            if (id == null)
            {
                RedirectToAction("Index");
            }
            string serverpath = Server.MapPath("~/Content/Files/");
            var filename = exelexporter.GetOrder(id, serverpath);
            // Объект Stream
            FileStream fs = new FileStream(serverpath + filename, FileMode.Open);
            string file_type = "application/xlsx";
            return File(fs, file_type, filename);
        }
        // GET: Orders/Create

        public ActionResult AutoOrderCreate()
        {
            var categorylistfromdb = db.PartCategories.ToList();

            var model = new AutoOrderCreateModel() { PartCategoryList = new List<Models.PartCategories.PartCategory>() };

            foreach (var item in categorylistfromdb)
            {
                var cat = new Models.PartCategories.PartCategory()
                {
                    Id = item.Id.ToString(),
                    PartCategoryName = item.Name
                };
                model.PartCategoryList.Add(cat);
            }

            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AutoOrderCreate([Bind(Include = "PartCategoryId")] AutoOrderCreateModel model)
        {
            var expectedparts = new List<PartExmple>();
            if (model.PartCategoryId == null)
            {
                expectedparts = db.Parts.Select(x => new PartExmple { PartNumber = x.PartNumber, Count = x.LimitQuantity }).Where(x => x.Count > 0).ToList();
            }
            else
            {
                expectedparts = db.Parts.Where(x => x.CategoryEntityId == model.PartCategoryId).
                           Select(x => new PartExmple { PartNumber = x.PartNumber, Count = x.LimitQuantity }).Where(x => x.Count > 0).ToList();
            }
            var exppartfinal = new List<PartExmple>();
            foreach (var item in expectedparts)
            {
                string partnumber = item.PartNumber;
                var al = expectedparts.Where(x => x.PartNumber == partnumber).ToList();
                if (al.Count > 1 && !(al.First().PartNumber == null))
                {
                    var expcount = al.Select(x => x.Count).Sum();
                    if (!exppartfinal.Select(x => x.PartNumber).Contains(partnumber))
                    {
                        exppartfinal.Add(new PartExmple { PartNumber = item.PartNumber, Count = expcount });
                    }
                }
                else
                {
                    var part = new PartExmple { PartNumber = item.PartNumber, Count = item.Count };
                    exppartfinal.Add(part);
                }
            }
            var restparts = fromexelexporter.GetRestParts();
            foreach (var expectedpart in exppartfinal)
            {
                foreach (var restpart in restparts)
                {
                    if (expectedpart.PartNumber == restpart.PartNumber)
                    {
                        expectedpart.Count = Convert.ToDouble(expectedpart.Count - restpart.Count);
                    }
                }
            }
            var w = exppartfinal.Where(x => x.Count > 0).ToList();
            var orderEntity = new OrderEntity()
            {
                Id = Guid.NewGuid(),
                Name = "AutoOrder ", //+ DateTime.Now,// + DateTime.Now.ToLocalTime(),
                OrderDate = DateTime.Now,
            };
            db.Orders.Add(orderEntity);
            int i = 0;
            foreach (var item in w)
            {
                OrderItemEntity orderItemEntity = new OrderItemEntity()
                {
                    Id = Guid.NewGuid(),
                    OrderEntityId = orderEntity.Id,
                    PartEntityId = db.Parts.Where(x => x.PartNumber == item.PartNumber).First().Id,
                    Count = Convert.ToDouble(item.Count),
                    SerialIndex = i++
                };
                db.OrderItems.Add(orderItemEntity);
            }

            db.SaveChanges();
            return RedirectToAction("Index");
        }
        public FileResult ExportTxtOrder(Guid id)
        {
            if (id == null)
            {
                RedirectToAction("Index");
            }
            string serverpath = Server.MapPath("~/Content/Files/");
            var filename = txtexporter.GetOrder(id, serverpath);
            // Объект Stream
            FileStream fs = new FileStream(serverpath + filename, FileMode.Open);
            string file_type = "application/xlsx";
            return File(fs, file_type, filename);
        }

        // POST: FindOrdersByItem
        [HttpPost]
        public async Task<ActionResult> FindOrdersByItem(string partnumber)
        {
            var query = from order in db.Orders
                        join meta in db.OrderItems on order.Id equals meta.OrderEntityId
                        where meta.PartEntity.PartNumber == partnumber
                        select new { order.Id, order.Name, order.OrderDate };
            var orders = new List<OrderViewModel>();
            foreach (var item in query)
            {
                orders.Add(new OrderViewModel() { Id = item.Id, Name = item.Name, OrderDate = item.OrderDate });
            }
            return View("Index", orders.OrderBy(x => x.OrderDate));
        }
        // POST: FindOrdersByItem
        [HttpPost]
        public async Task<ActionResult> FindOrdersByName(string partname)
        {
            var query = from order in db.Orders
                        join meta in db.OrderItems on order.Id equals meta.OrderEntityId
                        where meta.PartEntity.RusName == partname
                        select new { order.Id, order.Name, order.OrderDate };
            var orders = new List<OrderViewModel>();
            foreach (var item in query)
            {
                orders.Add(new OrderViewModel() { Id = item.Id, Name = item.Name, OrderDate = item.OrderDate });
            }
            return View("Index", orders.OrderBy(x => x.OrderDate));
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
