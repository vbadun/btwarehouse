﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Warehouse.Models;

namespace Warehouse.Controllers
{
    public class UnitCountController : Controller
    {
        private PartContext db = new PartContext();
        // GET: UnitCount
        public ActionResult Index()
        {
            var model = db.UnitCounts.Select(x=> new UnitCountModel() { Id= x.Id,  UnitCountName= x.UnitCountName }).ToList();
            return View(model);
        }

        // GET: UnitCount/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: UnitCount/Create
        public ActionResult Create()
        {

            return View();
        }

        // POST: UnitCount/Create
        [HttpPost]
        public ActionResult Create([Bind(Include = "UnitCountName")]UnitCountModel collection)
        {
            try
            {
               
                db.UnitCounts.Add(new DataAccess.Entities.UnitCountEntity() { Id = Guid.NewGuid(), UnitCountName = collection.UnitCountName });
                db.SaveChanges();
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: UnitCount/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: UnitCount/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: UnitCount/Delete/5
        //public ActionResult Delete(int id)
        //{
        //    return View();
        //}

        //// POST: UnitCount/Delete/5
        //[HttpPost]
        //public ActionResult Delete(int id, FormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add delete logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}
    }
}
