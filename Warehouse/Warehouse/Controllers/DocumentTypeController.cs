﻿using DataAccess;
using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Warehouse.Repository;
using Warehouse.Models.MOL;
using Warehouse.Models.DocumentType;

namespace Warehouse.Controllers
{
    public class DocumentTypeController : Controller
    {
        private UnitOfWork<PartContext> unitOfWork = new UnitOfWork<PartContext>();
        private GenericRepository<DocumentTypeEntity> repository;
        //private IPartRepository partRepository;
        public DocumentTypeController()
        {
            //If you want to use Generic Repository with Unit of work
            repository = new GenericRepository<DocumentTypeEntity>(unitOfWork);
            //If you want to use Specific Repository with Unit of work
            // partRepository = new PartRepository(unitOfWork);
        }
        // GET: Conto
        public ActionResult Index()
        {
            var list = new List<DocumentTypeViewModel>();
            var docs = repository.GetAll();
            foreach (var item in docs)
            {
                var doc = new DocumentTypeViewModel()
                {
                    Id = item.Id,
                    Name = item.Name
                };
                list.Add(doc);
            }
            return View(list);
        }

        // GET: MOL/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: MOL/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: MOL/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here
                var doc = new DocumentTypeEntity() { Id = Guid.NewGuid() };

                UpdateModel(doc, collection);
                unitOfWork.CreateTransaction();
                if (ModelState.IsValid)
                {
                    repository.Insert(doc);
                    unitOfWork.Save();
                    unitOfWork.Commit();
                    return RedirectToAction("Index");
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: MOL/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: MOL/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: MOL/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: MOL/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
