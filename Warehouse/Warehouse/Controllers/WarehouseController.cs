﻿using DataAccess;
using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Warehouse.Models.Balance;
using Warehouse.Models.Conto;
using Warehouse.Models.MOL;
using Warehouse.Models.Warehouse;
using Warehouse.Repository;

namespace Warehouse.Controllers
{
    public class WarehouseController : Controller
    {
        private UnitOfWork<PartContext> unitOfWork = new UnitOfWork<PartContext>();
        private GenericRepository<WarehouseEntity> repository;
        private GenericRepository<BalanceEntity> balancerepository;
        private GenericRepository<ContoEntity> contorepository;
        private GenericRepository<MOLEntity> molrepository;
        private IWarehouseRepository warehouseRepository;
        public WarehouseController()
        {
            //If you want to use Generic Repository with Unit of work
            repository = new GenericRepository<WarehouseEntity>(unitOfWork);
            contorepository = new GenericRepository<ContoEntity>(unitOfWork);
            balancerepository = new GenericRepository<BalanceEntity>(unitOfWork);
            molrepository = new GenericRepository<MOLEntity>(unitOfWork);
            //If you want to use Specific Repository with Unit of work
            warehouseRepository = new WarehouseRepository(unitOfWork);
        }
        // GET: Warehouse
        public ActionResult Index()
        {
            var list = new List<WarehouseViewModel>();
            var whs = warehouseRepository.GetAllWarehouses();
            foreach (var item in whs)
            {
                var wh = new WarehouseViewModel()
                {
                    Id = item.Id,
                    Name = item.Name,
                    BalanceName = item.Balance.Name,
                    ContoName = item.Conto.Name,
                    MOLName = item.MOL.Name
                };
                list.Add(wh);
            }
            return View(list);
        }


        // GET: Warehouse/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Warehouse/Create
        public ActionResult Create()
        {
            var wh = repository.GetAll();
            var contos = contorepository.GetAll().Select(x => new ContoViewModel() { Id = x.Id, Name = x.Name }).ToList();
            var mols = molrepository.GetAll().Select(x => new MOLViewModel() { Id = x.Id, Name = x.Name }).ToList();
            var bals = balancerepository.GetAll().Select(x => new BalanceViewModel() { Id = x.Id, Name = x.Name }).ToList();
            var model = new WarehouseEditModel()
            {
                ContoList = contos,
                BalanceList = bals,
                MOLList = mols
            };
            return View(model);
        }

        // POST: Warehouse/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {

            try
            {
                // TODO: Add insert logic here
                var wh = new WarehouseEntity()
                {
                    Id = Guid.NewGuid(),
                    Name = collection["Name"],
                    BalanceEntityId = Guid.Parse(collection["Balance.Id"]),
                    ContoEntityId = Guid.Parse(collection["Conto.Id"]),
                    MolEntityId = Guid.Parse(collection["MOL.Id"])
                };

                unitOfWork.CreateTransaction();
                if (ModelState.IsValid)
                {
                    repository.Insert(wh);
                    unitOfWork.Save();
                    unitOfWork.Commit();
                    return RedirectToAction("Index");
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Warehouse/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Warehouse/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Warehouse/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Warehouse/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
