﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Models
{
   public class BillItem
    {
        public Guid Id { get; set; }
        public Guid BillId { get; set; }
        [Display(Name = "№")]
        public int Number { get; set; }

        public DateTime? CreateTime { get; set; }
        [Display(Name = "Наименование")]
        public string PartName { get; set; }
        [Display(Name = "Артикул")]
        public string PartNumber { get; set; }
        [Display(Name = "Количество")]
        public string Count { get; set; }
    }
}
