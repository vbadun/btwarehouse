﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Models
{
   public class Bill
    {
        public Guid Id { get; set; }
        [Display(Name = "№")]
        public int Number { get; set; }
        [Display (Name="Дата создания")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime DateOfCreate { get; set; }

        //Relationships
        [Display(Name = "Заказчик")]
        public string CustomerName { get; set; }

      
    }
}
