﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Models
{
    public class Customer
    {
        public Guid Id { get; set; }
        [Display(Name = "Наименование ")]
        public string CustomerName { get; set; }
    }
}
