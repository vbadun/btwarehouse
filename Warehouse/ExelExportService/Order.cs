﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExelExportService
{
    public class Order
    {
        public string OrderName { get; set; }
        public IEnumerable<Part> Parts { get; set; }
    }
}
