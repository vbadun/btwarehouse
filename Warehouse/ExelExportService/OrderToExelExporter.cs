﻿using Aspose.Cells;
using DataAccess;
using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ExelExportService
{
    public class OrderToExelExporter : IExelExport
    {
        private PartContext db = new PartContext();


        public string GetOrder(Guid orderid, string serverpath)
        {
            var order_items = db.Orders.Where(x => x.Id == orderid).SelectMany(x => x.OrderItems);
            var parts_list = new List<Part>();
            foreach (var item in order_items)
            {
                var sel_part = new Part()
                {
                    PartEngName = item.PartEntity.EngName,
                    Count = item.Count,
                    PartNumber = item.PartEntity.PartNumber,
                    PartRusName = item.PartEntity.RusName,
                    SerialIndex = item.SerialIndex,
                    UnitCount = item.PartEntity.UnitCount.UnitCountName
                };
                parts_list.Add(sel_part);
            }
            var list = parts_list.OrderBy(x => x.SerialIndex).ToList();

            Order order = new Order()
            {
                OrderName = db.Orders.Where(x => x.Id == orderid).FirstOrDefault().Name,
                Parts = list
            };

            Workbook orderbook = new Workbook();
            Worksheet btworksheet = orderbook.Worksheets[0];
            ImportTableOptions imp = new ImportTableOptions();
            imp.InsertRows = true;
            imp.IsFieldNameShown = false;
            btworksheet.Cells.ImportCustomObjects(list.Select(x => new { x.PartRusName, x.PartEngName, x.PartNumber, x.Count, x.UnitCount }).ToList(), 10, 2, imp);
            btworksheet.Cells[5, 2].PutValue(order.OrderName);
            btworksheet.Cells[9, 1].PutValue("№");
            btworksheet.Cells[9, 2].PutValue("Нименование");
            btworksheet.Cells[9, 3].PutValue("Name");
            btworksheet.Cells[9, 4].PutValue("Артикул");
            btworksheet.Cells[9, 5].PutValue("Количество");
            btworksheet.Cells[9, 6].PutValue("Ед.изм.");
            for (int i = 0; i < list.Count; i++)
            {
                btworksheet.Cells[i + 10, 1].PutValue(i + 1);
            }
            // Auto-fit all the columns
            orderbook.Worksheets[0].AutoFitColumns();
            var name = db.Orders.Where(x => x.Id == orderid).SingleOrDefault().Name + ".xlsx";
            var filename = serverpath + name;
            //var filename = order.OrderName + ".xlsx";
            // Save the Excel file
            orderbook.Save(filename);
            return name;

        }
    }
}
