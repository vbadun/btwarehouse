﻿using Aspose.Cells;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExelExportService
{
    public class RestPartsFromExel : IExportFromExel
    {
        public List<PartExmple> partExmples { get; set; }
        public List<PartExmple> GetRestParts()
        {
            Workbook orderbook = new Workbook(@"D:\1\lely.xlsx");
            Worksheet worksheet = orderbook.Worksheets[0];
            ExportTableOptions to = new ExportTableOptions();
            to.PlotVisibleColumns = true;

            int totalRows = worksheet.Cells.MaxRow+1;
            int totalColumns = worksheet.Cells.MaxColumn;

            var parts = worksheet.Cells.ExportArray(0, 1, totalRows, totalColumns);
            var list = new List<PartExmple>();

            for (int i = 0; i < totalRows; i++)
            {
                if (parts[i,0] != null)
                {
                   // var numb = parts[i, 0].ToString().Trim();
                   // var count = Convert.ToDouble(parts[i, 1]);
                     list.Add(new PartExmple { PartNumber = parts[i, 0].ToString().Trim(), Count = Convert.ToDouble(parts[i, 1]) });
                }
            }
            var exppartfinal = new List<PartExmple>();
            foreach (var item in list)
            {
                string partnumber = item.PartNumber;
                var al = list.Where(x => x.PartNumber == partnumber).ToList();

                if (al.Count > 1 && !(al.First().PartNumber == null))
                {
                    var expcount = al.Select(x => x.Count).Sum();
                    if (!exppartfinal.Select(x => x.PartNumber).Contains(partnumber))
                    {
                        exppartfinal.Add(new PartExmple { PartNumber = item.PartNumber, Count = expcount });
                    }
                }
                else
                {
                    var part = new PartExmple { PartNumber = item.PartNumber, Count = item.Count};
                    exppartfinal.Add(part);
                }
            }
            return exppartfinal;
        }
    }
}
