﻿using Aspose.Cells;
using DataAccess;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExelExportService
{
    public class OrderToTxtService : ITxtExport
    {
        private PartContext db = new PartContext();


        public string GetOrder(Guid orderid, string serverpath)
        {
            var orderitems = db.OrderItems.Where(x => x.OrderEntityId == orderid).ToList();
            var parts = new List<Part>();
            foreach (var item in orderitems)
            {
                var partselected = db.Parts.Where(x => x.Id == item.PartEntityId).Include(x => x.OrderItems).Select(x =>
                  new { x.EngName, x.PartNumber, x.RusName, x.OrderItems.Where(p => p.OrderEntityId == orderid).FirstOrDefault().Count }).SingleOrDefault();
                var part = new Part()
                {
                    Count = partselected.Count,
                    PartEngName = partselected.EngName,
                    PartNumber = partselected.PartNumber,
                    PartRusName = partselected.RusName
                };
                parts.Add(part);
            };

            Order order = new Order()
            {
                OrderName = db.Orders.Where(x => x.Id == orderid).FirstOrDefault().Name,
                Parts = parts
            };

            Workbook orderbook = new Workbook();
            Worksheet btworksheet = orderbook.Worksheets[0];

            ImportTableOptions imp = new ImportTableOptions();
            imp.InsertRows = true;
            imp.IsFieldNameShown = false;
            btworksheet.Cells.ImportCustomObjects(parts.OrderByDescending(x => x.PartNumber).ToList(), 10, 2, imp);
            btworksheet.Cells[5, 2].PutValue(order.OrderName);
            btworksheet.Cells[9, 1].PutValue("№");
            btworksheet.Cells[9, 2].PutValue("Нименование");
            btworksheet.Cells[9, 3].PutValue("Name");
            btworksheet.Cells[9, 4].PutValue("Артикул");
            btworksheet.Cells[9, 5].PutValue("Количество");
            for (int i = 0; i < parts.Count; i++)
            {
                btworksheet.Cells[i + 10, 1].PutValue(i + 1);
            }
            // Auto-fit all the columns
            orderbook.Worksheets[0].AutoFitColumns();
        
            
            var name = db.Orders.Where(x => x.Id == orderid).SingleOrDefault().Name + ".txt";
            var filename = serverpath + name;
            //var filename = order.OrderName + ".xlsx";
            // Save the Excel file

            orderbook.Save(filename, SaveFormat.TabDelimited);

            return name;

        }
    }
}
