﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExelExportService
{
    public interface IExelExport
    {
        string GetOrder(Guid id, string path);
    }
}
