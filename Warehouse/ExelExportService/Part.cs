﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExelExportService
{
    public class Part
    {
        public string PartRusName { get; set; }
        public string PartEngName { get; set; }
        public string PartNumber { get; set; }
        public double Count { get; set; }
        public string UnitCount { get; set; }
        public int? SerialIndex { get; set; }
    }
}
