namespace Warehouse.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class inittestcustomers : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TestCustomer",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.TestCustomer");
        }
    }
}
