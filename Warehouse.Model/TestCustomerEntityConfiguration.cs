﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warehouse.Model
{
    class TestCustomerEntityConfiguration : EntityTypeConfiguration<TestCustomer>
    {
        public TestCustomerEntityConfiguration()
        {
            ToTable("TestCustomer");
        }
    }
}
