﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Warehouse.Model
{
    public interface IEntity<T>
    {
        T Id { get; set; }
    }
}
