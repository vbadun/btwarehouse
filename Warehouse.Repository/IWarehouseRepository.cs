﻿using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warehouse.Repository
{
    public interface IWarehouseRepository : IGenericRepository<WarehouseEntity>
    {
        IEnumerable<WarehouseEntity> GetAllWarehouses();
    }
}
