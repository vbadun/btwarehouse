﻿using DataAccess;
using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warehouse.Repository
{
    public class PartRepository : GenericRepository<PartEntity>, IPartRepository
    {
        public PartRepository(IUnitOfWork<PartContext> unitOfWork)
: base(unitOfWork)
        {
        }
        public PartRepository(PartContext context)
        : base(context)
        {
        }
        public IEnumerable<PartEntity> GetPartsByName(string Name)
        {
            var searchName = Name.ToLower();
            return Context.Parts.Where(part => part.RusName.ToLower().Contains(searchName)).ToList();
        }
        public IEnumerable<PartEntity> GetPartsByPartNumber (string Number)
        {
            var searchNumber = Number.ToLower();
            return Context.Parts.Where(part => part.RusName.ToLower().Contains(searchNumber)).ToList();
        }
    }
}
