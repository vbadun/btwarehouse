﻿using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warehouse.Repository
{
    public interface IPartRepository : IGenericRepository<PartEntity>
    {
        IEnumerable<PartEntity> GetPartsByName(string Name);
        IEnumerable<PartEntity> GetPartsByPartNumber(string Number);
    }
}
