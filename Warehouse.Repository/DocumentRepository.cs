﻿using DataAccess;
using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warehouse.Repository
{
    public class DocumentRepository : GenericRepository<DocumentEntity>, IDocumentRepository
    {
        public DocumentRepository(IUnitOfWork<PartContext> unitOfWork)
: base(unitOfWork)
        {
        }
        public DocumentRepository(PartContext context)
        : base(context)
        {
        }
        public DocumentEntity GetDocumentDetails(Guid id)
        {
            var doc = Context.Documents.Where(x => x.Id == id).FirstOrDefault();

            return doc;
        }
    }
}
