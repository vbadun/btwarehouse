﻿using DataAccess;
using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warehouse.Repository
{
    public class WarehouseRepository : GenericRepository<WarehouseEntity>, IWarehouseRepository
    {
        public WarehouseRepository(IUnitOfWork<PartContext> unitOfWork)
: base(unitOfWork)
        {
        }
        public WarehouseRepository(PartContext context)
        : base(context)
        {
        }

        public IEnumerable<WarehouseEntity> GetAllWarehouses()
        {
            var whs = Context.Warehouses.Include("MOL").Include("Conto").Include("Balance").ToList();
            return whs;
        }

        //public IEnumerable<DocumentItemEntity> GetDocumentDetails(Guid id)
        //{

        //    var doc1 = Context.DocumentItems.Include("Part").Include("Document.Mol").Include("Document.Conto").Include("Document.Warehouse").Where(x => x.Document.Id == id).ToList();
        //    //     var doc = Context.DocumentItems.Where(x => x.Document.Id == id).Select( x =>new {x.Part.RusName,x.Amount, x.Id });
        //    return doc1;
        //}
    }
}

